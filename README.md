# sisfokol-PPDB-Minat-Bakat

SISFOKOL-PPDB-MINAT-BAKAT v1.0 : Sistem Penerimaan Peserta Siswa Baru, berdasarkan Tes Minat Bakat. 

Cocok untuk sekolah yang sedang menerapkan ujian minat bakat, bagi calon peserta didik baru. Agar bisa sesuai jurusan minat yang akan dipilih.


Dibuat dan dites dengan WebServer XAMPP PHP 7.4 . Pada LinuxMint.


---


FITUR :

- Registrasi Calon

- Login Calon

- Ujian Tes Minat Bakat

- Akses Admin, Setting, Rekap dan Laporan.

- Set Diterima bisa ditentukan lagi secara edit data calon.

- Rekap Jawaban.






---

TAMPILAN / SKRINSUT :  

<p>
<img src="tmp_tampilan/image1.png" width="100%"> 
</p>


<p>
<img src="tmp_tampilan/image1.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image2.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image3.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image4.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image5.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image6.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image7.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image8.png" width="100%"> 
</p>

<p>
<img src="tmp_tampilan/image9.png" width="100%"> 
</p>


<p>
<img src="tmp_tampilan/image10.png" width="100%"> 
</p>


<p>
<img src="tmp_tampilan/image11.png" width="100%"> 
</p>




---

INSTALASI DAN KONFIGURASI :  

1. Ekstrak file web ke folder web webserver www atau htdocs

2. Untuk konfigurasi, bisa set di file /inc/config.php

3. Jalankan phpmyadmin, buatlah sebuah database. dan lakukan import file database .sql, ada di folder /db

4. Jalankan SISFOKOL-PPDB-MINAT-BAKAT sesuai alamat web yang ada.




---

http://alamat_web_nya/admin

CONTOH AKSES USER ADMIN :

User : admin  

Pass : admin




---

http://alamat_web_nya/ujian.php


CONTOH AKSES USER CALON :

User : 0042

Pass : 0042







---

NB. 

DEMO ONLINE atau ingin request custom konten berbayar, silahkan bisa hubungi : 

hp/sms/wa : 081-829-88-54 

atau email : hajirodeon@gmail.com



