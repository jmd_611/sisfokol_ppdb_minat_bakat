-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 11 Bulan Mei 2022 pada 11.12
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfokol_ppdb_minat_bakat`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `adminx`
--

CREATE TABLE `adminx` (
  `kd` varchar(50) NOT NULL,
  `usernamex` varchar(50) DEFAULT NULL,
  `passwordx` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `adminx`
--

INSERT INTO `adminx` (`kd`, `usernamex`, `passwordx`, `nama`, `postdate`) VALUES
('e807f1fcf82d132f9bb018ca6738a19f', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', '2019-12-23 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_visitor`
--

CREATE TABLE `cp_visitor` (
  `kd` varchar(50) NOT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `online` enum('true','false') DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `cp_visitor`
--

INSERT INTO `cp_visitor` (`kd`, `ipnya`, `postdate`, `online`) VALUES
('7693cfb7fe78acc55063de1f6793c09c', '127.0.0.1', '2022-01-11 11:19:15', 'false'),
('98949a937dac5617fb8ee7f81cfa4645', '127.0.0.1', '2022-01-11 11:19:21', 'false'),
('89cef7c2353d4951c3752f1abfdb5260', '127.0.0.1', '2022-01-11 11:19:22', 'false'),
('8a657063e9e85d8bb06305f0c2b5484c', '127.0.0.1', '2022-01-11 11:21:57', 'false'),
('9ded2eb649ac8052c54f192e7c16632a', '127.0.0.1', '2022-01-11 11:24:26', 'false'),
('eabd533592dc77a9961f28f2409a9547', '127.0.0.1', '2022-01-11 11:24:44', 'false'),
('293557fabf9d077b2aea27a662180387', '118.99.112.2', '2022-01-11 12:17:00', 'true'),
('5da953063a08bb5b8d101c5e6bf7b339', '118.99.112.2', '2022-01-11 12:17:02', 'true'),
('6bc078f4574adf09d3e5ad4537c49c57', '118.99.112.2', '2022-01-11 12:17:25', 'true'),
('cebc1aea7fe6d7c73abb5776ddba2fae', '34.138.228.194', '2022-01-11 12:40:16', 'true'),
('c7189fd0c6a5c7a03891ae989c59a943', '103.105.28.147', '2022-01-12 09:10:16', 'true'),
('191852703813d8af7a60be40dc0831cf', '36.72.216.86', '2022-01-13 15:37:58', 'false'),
('521f29bef7480054aa4373730fb43750', '36.72.216.86', '2022-01-13 15:38:15', 'false'),
('b8526f83040fcdc73cd6416f02fd0d0a', '140.213.59.222', '2022-01-13 17:26:52', 'true'),
('de47b11ff44db1480be632b6dcde8bca', '140.213.59.222', '2022-01-13 17:27:28', 'true'),
('4f6bad68e78935883371aea10b51005d', '35.196.97.11', '2022-01-14 04:57:34', 'false'),
('8a9aab1c4b8e5229699297e110f7ddf0', '185.119.81.97', '2022-01-14 06:53:23', 'false'),
('d697dc564bd265e231cbb5b7ff89d885', '34.138.220.227', '2022-01-14 07:21:45', 'false'),
('8a636c92dec865a18c8008b9a2ad89e6', '112.215.240.67', '2022-01-14 10:40:28', 'false'),
('e96f06768f4e0d45f17c3ce22447b036', '103.105.28.167', '2022-01-14 12:59:42', 'true'),
('5c06adb63153c16696b52fe9d60cf047', '118.99.112.2', '2022-01-15 09:01:37', 'false'),
('96024085d57eaf17c6e7f4e6f6d75fd8', '192.168.146.228', '2022-01-15 17:40:26', 'true'),
('51ef8c042dc4945acfa97f88e120026d', '192.168.146.228', '2022-01-15 17:41:55', 'true'),
('e6b1281f27904405b912aa7b44a959af', '192.168.146.228', '2022-01-15 17:43:09', 'true'),
('868bcaac664b321f32c245aef45c6962', '35.190.170.80', '2022-01-16 01:54:30', 'false'),
('23e639534bd70ee208ad3e3c2e55aa3d', '192.99.18.136', '2022-01-16 10:16:16', 'false'),
('5f1848008b5edcf7c8f9b7968c1e6c46', '36.79.83.172', '2022-01-16 16:55:47', 'false'),
('f27def035c73a124fe6a0c9bb9be127b', '36.79.83.172', '2022-01-16 16:55:55', 'false'),
('f3377845b46e1cf327546a53502a0b34', '36.79.83.172', '2022-01-16 17:02:58', 'true'),
('446b051bf156afb85a3c74a2a19ded49', '36.79.83.172', '2022-01-16 17:08:31', 'true'),
('0d7eec4bec52216c8cab8609b9866a5c', '114.142.170.50', '2022-01-17 02:01:53', 'false'),
('d8d0da6e14cf7ddace762b719e6af870', '158.140.170.35', '2022-01-17 11:27:17', 'false'),
('c14b94a0d6e62c01a3bc50363d85106a', '112.215.240.180', '2022-01-17 14:32:23', 'false'),
('8078d680bea3333ff2a6128bbf72eaf9', '54.161.98.96', '2022-01-17 17:47:31', 'true'),
('3cc004e7e02971e26737cfdffd2b4e2b', '185.119.81.97', '2022-01-18 13:56:23', 'false'),
('33785d199934a4d450539bc2b5b968f4', '35.237.170.97', '2022-01-18 19:43:40', 'true'),
('6d5b433c7318bfa48663d491c6b67016', '103.105.35.75', '2022-01-19 14:31:08', 'true'),
('5708cc3114c1e15a55517bf34e08d12d', '95.163.255.44', '2022-01-20 09:00:17', 'true'),
('36d6396c6a02851fe16220b93b54901c', '52.207.58.82', '2022-01-21 05:06:09', 'false'),
('daa68664568ad7fe6a4882c03b926631', '185.119.81.97', '2022-01-21 10:49:33', 'false'),
('8bc263f77791ddf1834f40dfb6a2793e', '103.105.27.116', '2022-01-21 23:46:28', 'true'),
('e2ddd38435a3876e75b5841044cc857d', '103.105.35.99', '2022-01-21 23:49:46', 'true'),
('ea84d2f7182dcf8007be3f96d4e5569c', '180.244.128.130', '2022-01-22 14:56:09', 'false'),
('c178fc37ad81e55f46d19aaa9710ad4c', '95.163.255.46', '2022-01-22 22:34:09', 'true'),
('a6a0e26f88bd63fbf13026ceac576a39', '103.105.27.72', '2022-01-23 10:31:14', 'false'),
('ae85217060910012db50c7b751cfa69f', '103.105.27.72', '2022-01-23 10:32:00', 'false'),
('71cc676c99d8e3c0a3abc9dff063cc4a', '103.105.27.72', '2022-01-23 10:33:54', 'false'),
('81a35010974179b427ca040ae3c1a3b3', '103.105.27.72', '2022-01-23 10:35:43', 'false'),
('8a2bb7b35586f2999d71a8a22671aa30', '157.90.209.76', '2022-01-23 23:31:17', 'true'),
('d197a27e3c334944b16bca9d18d3000d', '36.72.216.65', '2022-01-24 02:39:40', 'false'),
('18d4dbd8779f9ad41f4a8ed6a76d972e', '54.36.148.132', '2022-01-24 05:56:10', 'false'),
('95d19ca530dea41963a104af95cf3752', '140.213.56.69', '2022-01-24 22:51:44', 'false'),
('72418b1d040f9ad68e4834b85478918c', '140.213.56.69', '2022-01-24 23:04:06', 'true'),
('4f5ae06c29369397aa2f3c1c0170ecf7', '34.139.17.141', '2022-01-25 01:02:03', 'false'),
('72f2f1318150885c990d44307b50e3c3', '114.79.20.241', '2022-01-25 07:40:26', 'false'),
('684acb23777931cfc109cd8c0b682622', '182.2.70.246', '2022-01-25 09:33:33', 'false'),
('d52812a40d0f9c67b39987d1c37a6329', '182.2.70.246', '2022-01-25 09:34:26', 'false'),
('b5b665dc0ce74af6a0ea7fbedcd5dca1', '34.139.115.110', '2022-01-25 10:26:34', 'false'),
('dcd3603cd6731dac39015d8bff60506d', '185.119.81.97', '2022-01-25 12:58:01', 'true'),
('0822c8bb3764e9ac54d2243ca7d060ea', '10.10.10.223', '2022-01-26 11:08:47', 'false'),
('b1729a574a5074ef02c624028b4654dd', '10.10.10.223', '2022-01-26 11:11:18', 'false'),
('0161682d7fe8a5d2fb699516948328a4', '103.105.35.108', '2022-01-26 14:58:25', 'false'),
('24724deab9ddf6cdce66ea36d3c37b13', '103.105.35.108', '2022-01-26 14:58:54', 'false'),
('f0b3075aa4a0e4991bf3a840ec07a425', '203.78.117.74', '2022-01-26 16:23:15', 'false'),
('b3956a4e80085fc3659bce3ed26c5ab8', '114.10.7.181', '2022-01-26 20:42:36', 'false'),
('b53cfeb5a7857858e17aa97911dce610', '36.72.219.219', '2022-01-26 21:44:38', 'true'),
('3a429f98fc1902787ac2073e9b2e9c6e', '36.72.219.219', '2022-01-26 21:45:15', 'true'),
('272a40ace5f35c207febf480b062b34b', '36.72.219.219', '2022-01-26 21:48:19', 'true'),
('38818f62068c83d50851370777a89a64', '182.253.62.109', '2022-01-27 05:26:13', 'false'),
('2af6359b9ec99fd833d809b2638473e1', '34.138.217.233', '2022-01-27 08:01:55', 'false'),
('5c674d69abc261ee5799f826cd45fd56', '118.99.112.2', '2022-01-27 10:38:41', 'false'),
('e8c7a51335476741f7efff597c30669b', '114.79.20.205', '2022-01-27 10:39:45', 'false'),
('20e0ae25ed7bcb317d5d06950a48a774', '103.105.28.184', '2022-01-27 10:41:20', 'false'),
('8b9e8c6319a086cfc82e5249279b6692', '103.105.28.184', '2022-01-27 10:42:03', 'false'),
('0f88169adf69401f2c90d9f604179224', '103.105.28.184', '2022-01-27 10:42:26', 'false'),
('c7c748ed047f64a58a08c1c4792c1110', '103.105.28.184', '2022-01-27 10:43:25', 'false'),
('599c00d8c02f25acf0be2c2ad8a4b71b', '112.215.244.144', '2022-01-27 10:54:19', 'false'),
('63eb1f3261109a43c956c5ad42274d67', '112.215.173.11', '2022-01-27 11:49:45', 'false'),
('f018594cc93635dc32586c5cb3ad4c42', '118.99.112.2', '2022-01-27 12:07:16', 'false'),
('f00e487d67f2db5706e7bd3cfd6fad92', '118.99.112.2', '2022-01-27 12:19:04', 'false'),
('a4518da4e63fe6d6c1f2b235fe56beca', '182.2.41.149', '2022-01-27 13:28:54', 'false'),
('ed2ca534622ee4fc749e5af626814ad6', '182.2.41.149', '2022-01-27 13:29:53', 'false'),
('d063777a1e9f7762f21e839d9c2168c1', '112.215.153.182', '2022-01-27 16:12:15', 'false'),
('e3659232da6039ab6b75243a997a8962', '112.215.153.30', '2022-01-27 18:48:56', 'false'),
('687488c393486d51eb586fc325bbb2e7', '185.191.171.37', '2022-01-27 20:12:49', 'true'),
('d3695e279fbb7cc325b5f22adeb5da8e', '54.210.204.73', '2022-01-27 20:15:29', 'true'),
('5482877694b5006d83ca3a6943bcd990', '54.210.204.73', '2022-01-27 20:15:44', 'true'),
('036791f563c97a7ad42288f03614d2b0', '112.215.237.138', '2022-01-28 19:22:34', 'false'),
('d62aee3193582210f1ccacf8b647e102', '112.215.237.138', '2022-01-28 19:23:26', 'false'),
('4b8bf562bd7ed262678c839fba8ccd51', '182.2.37.50', '2022-01-28 19:29:49', 'false'),
('5c2a2672dd1b439f52cefc78f46d98e8', '36.72.217.113', '2022-01-28 19:33:13', 'false'),
('ead3865ba73c1fa4268b8fc2c6d775c1', '36.72.217.113', '2022-01-28 19:33:55', 'false'),
('642c98b6bd260379e1a2b305de0ca10c', '140.213.59.25', '2022-01-28 19:38:47', 'false'),
('034d9e057ad19e68348920ac305716dc', '112.215.243.25', '2022-01-28 19:39:44', 'false'),
('e5868219f8e85e50466792dfd12ec90a', '112.215.243.25', '2022-01-28 19:40:57', 'false'),
('b788541d4286171d26ad3077777655d6', '182.2.37.67', '2022-01-28 19:47:52', 'false'),
('26783aa844b0a0904ec632119b92468e', '182.2.40.12', '2022-01-28 19:55:51', 'false'),
('9462fb6bc87f7d7c25ccc07e2424252e', '114.10.17.83', '2022-01-28 19:56:45', 'false'),
('5e293f97dd50d8c7e083c0e6c3fc1acd', '103.124.137.203', '2022-01-28 20:31:15', 'false'),
('974d6ff53c21ce97b9ac7793d35a34e8', '112.215.243.160', '2022-01-28 21:31:34', 'true'),
('3c73a3a309cd313a555a3e2073088d5a', '112.215.243.160', '2022-01-28 21:31:56', 'true'),
('38cabe53fce68faa663e0f94b01adae3', '182.253.62.109', '2022-01-29 03:34:12', 'false'),
('e5637f77fb49827c49363848e8953ced', '182.253.62.109', '2022-01-29 04:55:17', 'false'),
('2b4b6b4d027afd8c529b1c84fc5b2974', '180.246.65.67', '2022-01-29 09:39:41', 'false'),
('ea7139613f9d1bad35149503e1c389cf', '114.10.7.181', '2022-01-29 11:35:15', 'false'),
('378978af76986ec8e9e6b92940f01d5a', '118.99.112.2', '2022-01-29 13:26:04', 'false'),
('a4f27c35c29eed43056a9741f8bfbc6d', '182.253.62.109', '2022-01-29 13:39:34', 'false'),
('46b30e6530acd57c11dcaecf655d33a4', '34.139.20.103', '2022-01-29 20:20:42', 'false'),
('92148956a6b118c56968fa2fae28005e', '54.36.148.132', '2022-01-29 21:28:47', 'true'),
('15ae00744a8f2263ac12a65bda777f2d', '142.132.143.108', '2022-01-30 20:46:27', 'true'),
('bc300333ca8c5d25ec292afcebfe13ed', '36.68.55.27', '2022-01-31 13:32:24', 'false'),
('bdce6c59faebb9cea7880139491a5f10', '36.68.55.27', '2022-01-31 13:32:25', 'false'),
('634f7ce07bedbdb4a3464fbec107a0c1', '36.68.55.27', '2022-01-31 13:32:26', 'false'),
('dd5eea8cf48f95b77c77310a2f57077b', '36.68.55.27', '2022-01-31 13:32:46', 'false'),
('0e973314478447a29ab0a666f42f232e', '36.68.55.27', '2022-01-31 13:32:51', 'false'),
('8d6d54be8ddba0feb9eccdb6bf452228', '36.68.55.27', '2022-01-31 13:44:01', 'false'),
('6e53314c870b941f26e5e06f894bc10d', '36.68.55.27', '2022-01-31 13:46:20', 'false'),
('18783cb013d50dfdef4c11163df83e04', '36.65.216.14', '2022-01-31 14:38:00', 'false'),
('3e41ed15fbf33f7ad8e079bc0047f016', '103.105.28.186', '2022-01-31 19:49:10', 'true'),
('e1f32492b6d4131a65830ad2aeb5dd19', '34.138.20.181', '2022-02-01 09:00:35', 'false'),
('a59665b6dc567399b532067582d74517', '182.2.69.23', '2022-02-01 19:28:58', 'true'),
('e32f2a8c8eb420fd596ad61006611508', '182.2.69.23', '2022-02-01 19:29:04', 'true'),
('19a04be14571d6cd2d257e94e68ff79c', '182.2.69.23', '2022-02-01 19:29:08', 'true'),
('f5aaf04b47977f7ed9a70d7a85d2bbcd', '182.2.69.23', '2022-02-01 19:29:11', 'true'),
('93f52f96c8beafe5782ea5056947b5e7', '182.2.69.23', '2022-02-01 19:29:23', 'true'),
('f0f0e0664397f1051762f70863c257dd', '182.2.69.23', '2022-02-01 19:29:31', 'true'),
('94444d0c2ada416a31506eafffe1ea21', '103.105.35.74', '2022-02-02 13:20:32', 'false'),
('2dc2a2d2bf50c52ac24c32352b383721', '103.105.35.74', '2022-02-02 13:21:12', 'false'),
('f42d233f8265caf9a061221421e01dfd', '3.234.252.137', '2022-02-02 13:30:51', 'false'),
('057d0454d02448e6e148e22fd7fb1f9c', '3.234.252.137', '2022-02-02 13:31:01', 'false'),
('2418b882169c92c31e842d846d8b02ad', '3.234.252.137', '2022-02-02 13:31:15', 'false'),
('2687b367c0c5dba25bdc5d3f6160302c', '103.105.28.136', '2022-02-02 13:41:48', 'false'),
('690e8be485f8ef38674a5a4318db6f44', '103.105.35.94', '2022-02-02 16:31:54', 'true'),
('a848eb8e1bf3e28d34f0221d14a74d4a', '103.105.35.94', '2022-02-03 05:16:32', 'false'),
('ace055809b27487f021b90b04fb75dd5', '185.119.81.97', '2022-02-03 09:39:05', 'false'),
('9e3b2f838bc0eccb4101248483a1c980', '36.68.54.60', '2022-02-03 21:47:54', 'true'),
('902ff4005c348c07b15ea80c54411a70', '36.68.54.60', '2022-02-03 21:48:03', 'true'),
('1a1b30f282519b314e478108bde5947c', '36.68.54.60', '2022-02-03 21:48:05', 'true'),
('c942762168a46a57098e1e72ae073acb', '36.68.54.60', '2022-02-03 21:48:07', 'true'),
('fda17b6977e7d7349812418eaa3f68cf', '36.68.54.60', '2022-02-03 21:48:10', 'true'),
('65bcf1ebc1b8bbb980bd985af42e4b57', '54.174.78.127', '2022-02-04 11:00:30', 'true'),
('f8142f78272974ddcd208e204131fd28', '54.36.148.132', '2022-02-05 00:33:30', 'false'),
('de5d03c0350684b9c9e88aff60245b58', '192.99.18.136', '2022-02-05 11:02:58', 'false'),
('4805ce6740bc6004b8ff34be98518082', '103.172.35.15', '2022-02-05 20:35:02', 'true'),
('aeb85e5e6d2b1781d07651ae2d2fc731', '114.10.5.93', '2022-02-06 12:59:39', 'false'),
('ecdeea8cf64a51e232cc3e038d4f9d0e', '114.10.5.93', '2022-02-06 13:01:14', 'false'),
('395e8aa10e790087e80f80cd8c0e289b', '140.213.166.159', '2022-02-06 15:06:56', 'true'),
('afd93c8044969a068e105da403d0df9c', '140.213.166.159', '2022-02-06 15:07:13', 'true'),
('855d4f96aebe528dad5f262e27858d93', '18.234.207.37', '2022-02-07 09:45:10', 'true'),
('77e85d748d739c3cd88a14f979d0d974', '18.234.207.37', '2022-02-07 09:45:24', 'true'),
('45802314a77a1d5d0dadb0a1327880b4', '182.253.8.176', '2022-02-08 05:07:03', 'false'),
('b0e7efd0b1863a6afb00e24344674f29', '35.196.68.181', '2022-02-08 05:19:11', 'false'),
('cbd8d8caef3fc11d00f9bbdfd4ec7358', '118.99.112.2', '2022-02-08 09:52:13', 'true'),
('18ded0de40a8562fceeb94bfc77511a6', '182.253.8.143', '2022-02-09 04:41:26', 'false'),
('e48636cd1bbcb3726c492ac29fdb0894', '182.3.6.238', '2022-02-09 12:23:34', 'false'),
('a359b11db6861ea26aa8af492cbd09d7', '157.90.209.76', '2022-02-09 15:20:24', 'true'),
('3704563e4d92f93d335f914bc21235a1', '138.201.197.246', '2022-02-10 12:53:47', 'true'),
('ebe5c4e756c403cc550eb3dcd9a0f83a', '54.36.148.132', '2022-02-11 02:01:17', 'false'),
('eb44fafe17c9a46afa02b217d5cde7eb', '103.105.28.175', '2022-02-11 08:05:01', 'false'),
('8fbb8e0dae1c9c9dd4d2e33f84fd6afb', '182.253.62.126', '2022-02-11 10:54:18', 'false'),
('fe3d0e0c09cec4d8f9394c52968be940', '118.99.112.2', '2022-02-11 11:19:31', 'true'),
('20af5a9795c5830b733ca8b390942637', '118.99.112.2', '2022-02-12 11:05:28', 'false'),
('560063dac825c12550a43289446b824f', '112.215.172.126', '2022-02-12 12:15:11', 'false'),
('423fe19943838f4af509e8133c2c73ec', '114.10.23.99', '2022-02-12 12:44:55', 'false'),
('80efde98246e55c1229574b98ae00830', '103.105.35.96', '2022-02-12 13:56:29', 'true'),
('78fb30af3cecc94b685faed9f02adea2', '34.139.169.156', '2022-02-13 07:22:15', 'false'),
('f9b0129c84790a68a40bb801aa1d2d18', '182.2.73.200', '2022-02-13 10:47:51', 'false'),
('bec5c67d9589c5127e5c1cbec195c2a7', '182.2.73.200', '2022-02-13 10:49:01', 'false'),
('13639be5666e1dcdb850f4b3f1c31d6a', '182.2.41.74', '2022-02-13 13:32:19', 'true'),
('de1f6a6fc7aab7aa8b388b3a4dae3c2d', '182.2.38.2', '2022-02-14 00:03:17', 'false'),
('e3d04a445d55f52273326ded43f5101e', '182.2.38.2', '2022-02-14 00:06:23', 'false'),
('5413a55e7588cb5ba6519265f360dbfb', '36.68.52.180', '2022-02-14 14:41:29', 'true'),
('fc6f2a9c229e0624cb7874a0ed844324', '103.105.35.112', '2022-02-15 03:02:21', 'false'),
('02696c883358be75f61e156fa3b6cc4e', '10.10.15.120', '2022-02-15 08:33:01', 'false'),
('8127f1b262bc218ef5bdefaffc0fee24', '103.105.28.136', '2022-02-15 13:56:31', 'true'),
('834366da189ba11f4e5e41566e61f33f', '185.119.81.97', '2022-02-16 08:19:26', 'false'),
('aee95e9a7fedd17b4944ebf068c6f11f', '54.36.148.132', '2022-02-16 17:38:31', 'false'),
('927677ba7d554e8a6fcd52c3e647e982', '36.73.102.23', '2022-02-16 18:40:01', 'true'),
('f2a557f365cc42509db2820c2219f3a3', '118.99.112.2', '2022-02-18 10:43:31', 'true'),
('164e29bb5ea88a85c0475d87e0bc761e', '216.151.184.110', '2022-02-19 18:16:05', 'true'),
('77404352947182f6f23ad74b23ae1666', '74.112.157.100', '2022-02-20 01:44:21', 'false'),
('2f9ba2003ca96647f01d2c844ea156d1', '185.191.171.12', '2022-02-20 16:10:18', 'false'),
('d138cac65ec8ea351a4ebbe17812d7c1', '157.90.209.76', '2022-02-20 20:31:01', 'true'),
('f50775026c3d8f623cedf35a6426024f', '103.105.35.82', '2022-02-21 20:03:44', 'true'),
('9301b4d93a0963523626484870355b96', '182.2.72.163', '2022-02-23 15:33:45', 'false'),
('7fbab2285479a1383cdcbac2cbc39c51', '182.2.72.163', '2022-02-23 15:33:46', 'false'),
('17d1049d23958255a437f605d0344fc6', '54.36.148.132', '2022-02-23 23:40:44', 'true'),
('52f1fae7250f3ecde09130ddfcb0c650', '18.117.93.30', '2022-02-25 07:09:46', 'true'),
('75fa30e7a11defaaeef061ffcfdb5d5c', '192.99.18.136', '2022-02-27 17:04:41', 'true'),
('5824a961a70e5e0ec78ec0124768b315', '176.9.137.17', '2022-02-28 03:18:54', 'false'),
('01cd441022461e985c2b3f80440f2d26', '114.10.23.93', '2022-02-28 07:39:27', 'false'),
('0d9391bd1dbb174de9c0c709259a6790', '154.54.249.197', '2022-02-28 20:37:40', 'true'),
('c13b108af35f9f8ec581438a29c049bf', '182.2.70.133', '2022-03-01 06:55:45', 'false'),
('6702c2c3e965e89b66f59afd1a3cc165', '203.78.118.53', '2022-03-01 13:42:21', 'false'),
('bb0e38b445886a83215d8c3b294bcfe6', '54.36.148.132', '2022-03-01 15:05:52', 'true'),
('f8fe06ca2fec670d254b63f8e6985d00', '36.71.83.247', '2022-03-01 15:49:20', 'true'),
('e8a8fe415665a779930fdff3ad82c0d0', '157.90.209.76', '2022-03-02 05:41:35', 'false'),
('191e1a8d46625a91cfaa370443e60b88', '182.2.75.229', '2022-03-02 06:07:17', 'true'),
('c6dce26a11ae0b4ca74563bf8a37a8bf', '182.2.39.110', '2022-03-03 08:00:03', 'true'),
('beaa3ab21d41fd9adf48bfd33111f395', '118.99.112.2', '2022-03-04 08:49:43', 'false'),
('5936950186b2dbdb2f158275cf5b0282', '46.101.9.216', '2022-03-04 19:41:27', 'true'),
('027315f8f4f5a6c1f9df8795fc08161a', '36.73.115.245', '2022-03-05 12:28:42', 'true'),
('b496904a6cb47ffef936e05a1346ee35', '54.234.232.92', '2022-03-05 12:31:28', 'true'),
('2af238d6b306bbc9fd83fc913594e2e6', '54.36.148.132', '2022-03-07 09:24:10', 'false'),
('69cd01dc70e7a893246248e5cfc912aa', '3.85.235.93', '2022-03-07 10:23:45', 'false'),
('6f5bd89f43d67b7e04483063378913aa', '35.208.135.131', '2022-03-07 12:16:02', 'false'),
('9985cbe5a507ac69cb1b08138e3ecea6', '192.168.149.1', '2022-03-07 23:01:29', 'true'),
('f4b31622bc747f8a0329f2d1aee01965', '120.188.82.88', '2022-03-09 10:45:32', 'false'),
('8bbe3a999f358a744162e4a0ddcae464', '103.105.35.104', '2022-03-09 12:09:45', 'false'),
('ff37b6be2b65cad5aedbacf82999fd5d', '185.191.171.34', '2022-03-09 23:27:41', 'true'),
('0174385145ac6f1e77f083f703cffcda', '185.191.171.36', '2022-03-12 02:47:50', 'true'),
('17115007cb1004f14a27052793cf88a7', '95.108.213.43', '2022-03-13 04:17:50', 'false'),
('f3d23251153f5ea84d43b4696c47c644', '87.250.224.23', '2022-03-13 04:34:11', 'false'),
('d2ee708121866dcf93e41b65c9729294', '87.250.224.21', '2022-03-13 04:34:54', 'false'),
('4d3302b82aa03a90cfcc41f8a36223a3', '54.36.148.132', '2022-03-13 11:18:15', 'false'),
('cf46f3daeaef86608d0d180d9ca5c799', '182.2.36.81', '2022-03-13 20:02:39', 'true'),
('0f4ebf074f2b208c488bbc2de293589f', '36.80.182.186', '2022-03-15 07:53:00', 'false'),
('6fbd6673eb49998e9590a4d2dd60cbd2', '180.253.174.133', '2022-03-15 18:24:35', 'true'),
('f13f584629eaa74e62e0339150135e33', '118.99.112.2', '2022-03-16 08:25:28', 'false'),
('a7375342e412c107900512bbdd016185', '34.74.129.96', '2022-03-16 14:32:11', 'true'),
('84ec9591d62b36d0d32734bf00f65f8e', '112.215.243.152', '2022-03-18 13:22:43', 'true'),
('cbfa58da489910fad046f1c13e34423c', '54.36.148.132', '2022-03-19 03:48:41', 'true'),
('82ff87607bcdc77860444e9156256290', '182.2.43.87', '2022-03-20 07:59:34', 'false'),
('6a75bbea38a9731b3ee986b779706189', '36.79.86.33', '2022-03-20 13:34:05', 'false'),
('80998c2a1970af8fa0d9576b3f269218', '5.188.211.21', '2022-03-20 18:16:48', 'true'),
('6f3466daa6f211ad2efa02db9a3b194e', '157.90.209.76', '2022-03-21 08:40:56', 'false'),
('f826ed76fdc619170bd3b6c6f891379f', '36.74.43.6', '2022-03-21 10:55:14', 'true'),
('21c2399f0860e94785ba34830c33a2bc', '36.79.39.101', '2022-03-22 13:46:48', 'true'),
('3ab3df97a4073823d49819a12c9a5991', '114.10.11.1', '2022-03-23 20:16:27', 'true'),
('4634fac6fd9f7aff8410872a5a000b42', '142.132.248.190', '2022-03-24 01:26:59', 'false'),
('8d5c91b400c9502a63ab4b1b4b04c9fe', '54.36.148.132', '2022-03-24 21:36:55', 'true'),
('32eb9fade777551416fbeee9746db28b', '27.122.14.91', '2022-03-27 23:30:59', 'true'),
('a9682e1772dfdde05a22dea36d718137', '118.99.112.2', '2022-03-28 08:11:09', 'true'),
('92e33675d39f6177fed22349562071c2', '114.10.6.194', '2022-03-28 08:47:19', 'true'),
('9f67077b5f07afab3201d5ec6755cbe4', '114.10.19.207', '2022-03-29 16:23:16', 'true'),
('367265c47c2fec8e789ff66a32254fb6', '118.99.112.2', '2022-03-30 11:46:06', 'false'),
('b86d244129f4224817e7719561b66971', '54.36.148.132', '2022-03-30 14:24:30', 'true'),
('c4a564a3bc23aeee721f91b97b90d727', '110.136.171.200', '2022-03-30 14:38:20', 'true'),
('367d33adaf891837c118d951af3a4caa', '154.54.249.197', '2022-03-31 17:30:41', 'false'),
('40c6cf6bda318f3531fb4bdb167d49d3', '103.105.35.67', '2022-03-31 18:15:20', 'true'),
('6892a072dd6e36b3dea3e23995898357', '103.105.35.67', '2022-03-31 18:15:30', 'true'),
('975deded91430ce467e95610bee5f657', '103.105.35.67', '2022-03-31 18:15:36', 'true'),
('5675b6bdb57554b892edf2c1f5a132ac', '103.105.35.67', '2022-03-31 18:15:41', 'true'),
('f85cdf575f32ee3660b430db8f9e1fd7', '103.105.35.67', '2022-03-31 18:15:48', 'true'),
('40ec43983413631eeb3b6d16369270f2', '142.132.143.105', '2022-04-03 23:06:52', 'true'),
('fe1963a2d8c756e2af06ce40af8261c0', '36.80.138.179', '2022-04-04 11:29:49', 'false'),
('7002cdfcb87e4631c0c91a422f74d622', '36.80.138.179', '2022-04-04 11:45:11', 'false'),
('280deabfbad7c679d8f3b997ab0487a8', '185.191.171.10', '2022-04-04 18:05:03', 'true'),
('872a70ce1f7ef90b6ba2136135bc0f52', '54.36.148.132', '2022-04-05 07:14:26', 'true'),
('d9aec4da6ecd7a087f662bf2b8ed2aef', '118.99.112.2', '2022-04-07 08:11:36', 'true'),
('23f014ffa21a9f507abaf6896c7fda7e', '157.90.209.76', '2022-04-08 00:08:23', 'false'),
('d20b3831f15597b6c85ff32b1c56c29c', '103.105.28.145', '2022-04-08 10:40:13', 'false'),
('7ebabf2d7145f5cb478a20e07ca7a86b', '36.71.85.16', '2022-04-08 10:45:27', 'false'),
('3ad0e654611148943d92f28f0e94c7a3', '182.253.62.124', '2022-04-08 13:55:17', 'true'),
('32b5ccd81a10934442f96673aea7395c', '182.253.62.124', '2022-04-08 13:55:28', 'true'),
('023742dbf9bffc3a8d09c8680bb53841', '182.253.62.124', '2022-04-08 13:55:36', 'true'),
('86b10d31ac20fc3e7faf05e99c55a64e', '140.213.42.153', '2022-04-09 10:23:55', 'false'),
('3d5754adca802d870a6868703b79a91a', '10.10.10.244', '2022-04-09 14:36:38', 'true'),
('c320ff334cc9ea18121de069ec9534aa', '54.36.148.132', '2022-04-11 02:54:09', 'true'),
('f60f45493822a81baa6c09a2378c3f69', '140.213.57.6', '2022-04-13 19:14:48', 'false'),
('e1d6c463f0cbabbc95e63ede074b9088', '36.71.81.114', '2022-04-13 21:32:59', 'false'),
('63d1da4665770bcef8f9b5681fd295a3', '157.90.209.76', '2022-04-13 23:58:02', 'true'),
('bff753c8f954f7c7d5aa95565956d5b1', '103.105.28.178', '2022-04-15 12:16:10', 'true'),
('d4ca2ac4e8bb10496d1ac1635c33eb53', '54.36.148.132', '2022-04-17 01:22:33', 'false'),
('34f1d084cbcfbf95c56e31d5e8f3aa3b', '192.99.18.122', '2022-04-17 04:31:42', 'true'),
('c6d20b5a1e08448be2e41f90402dce20', '5.188.211.16', '2022-04-19 11:25:13', 'true'),
('5da5dc0cd23af1e76f6be36cbabcfd29', '5.188.211.16', '2022-04-19 11:25:13', 'true'),
('d60abd8ae6889ad2c8d737a1394afc8e', '114.5.242.249', '2022-04-22 04:34:54', 'false'),
('4e7122b41e61f414fe2cd362d2243e66', '114.10.21.73', '2022-04-22 18:23:25', 'true'),
('b372ef19149baa356c081cc1b646dcb1', '114.10.21.73', '2022-04-22 18:23:27', 'true'),
('366367be48bb7f9dbd6a575b0f5e245b', '114.10.21.73', '2022-04-22 18:23:54', 'true'),
('bbaa789a2aa0f52fa5d47b3c3ba94d19', '54.36.148.132', '2022-04-23 05:29:00', 'false'),
('2383971407d7daf9a97851f1d251fc81', '185.191.171.13', '2022-04-23 10:24:53', 'true'),
('ec49b58a05f8711fe7beace05c6d2cfa', '36.80.138.98', '2022-04-24 10:46:17', 'false'),
('d2c7418af4cc975dd24b9e3e9bdc2ba5', '182.1.105.55', '2022-04-24 19:46:46', 'true'),
('7f9fff38e1ec7cb175567c7b887bc99c', '185.191.171.15', '2022-04-25 17:00:42', 'false'),
('8d9e3d0002ef8cd498434b936e338899', '185.191.171.15', '2022-04-25 17:01:33', 'false'),
('1ef5c83252e1bd7953368851c1d03af0', '114.10.19.176', '2022-04-25 20:41:26', 'true'),
('0aff165fb4d13b3b067f24eb83ca477b', '36.71.84.137', '2022-04-26 18:18:34', 'true'),
('05d3c6b5350d10b23b611bb147d6d6c5', '36.71.84.137', '2022-04-26 18:18:37', 'true'),
('48696583150b4fda82f0055adec35f7f', '182.2.36.52', '2022-04-27 21:06:42', 'true'),
('3c3b40ab992ab4af8a11f2a342d886ac', '182.2.36.52', '2022-04-27 21:06:43', 'true'),
('b656b1a7046abeef7d0089c2a05bd982', '114.4.4.196', '2022-04-27 21:06:46', 'true'),
('8ee23483a7aab246c8a61cc31aea87df', '182.2.70.245', '2022-04-28 21:51:00', 'true'),
('7eb8a63fcaaeb799efba1d73716676f6', '54.36.148.132', '2022-04-29 07:53:55', 'false'),
('8a308bee292d636037dcb4053e3bfbcc', '182.2.75.188', '2022-04-29 20:28:38', 'true'),
('3a28ed102aaa2244e82ed414c1c71ce3', '103.144.170.59', '2022-04-30 00:12:41', 'true'),
('8f8eebc117c17b8a518feba0f2c58c94', '125.163.175.54', '2022-05-04 22:58:38', 'true'),
('4d410b2cd175f5980df4a357aafa37dd', '103.105.27.82', '2022-05-05 16:39:52', 'false'),
('21936f917e511be688b21ec9b0901def', '150.129.59.4', '2022-05-05 16:39:56', 'false'),
('139dd762885b21edb3c7f34185da2e14', '54.36.148.132', '2022-05-05 19:30:32', 'true'),
('3171df888279dd2fc99961b2b98e4cb6', '87.250.224.24', '2022-05-06 03:08:30', 'true'),
('1a63eb4e9f1afe33879e7ecef0c1eda6', '182.3.40.200', '2022-05-07 19:51:20', 'true'),
('17836aadde4958e0de92e87a6efde236', '182.3.40.200', '2022-05-07 19:51:47', 'true'),
('7cf2ade29636504a31310e9a92b832bd', '182.3.40.200', '2022-05-07 19:57:57', 'true'),
('82b5738c5421ecfac90da4b56654e032', '182.3.40.200', '2022-05-07 19:58:31', 'true'),
('99c2ec44938382024dead4f9a4bec86c', '182.3.40.200', '2022-05-07 19:58:48', 'true'),
('8fe1da648c011b1e20b5481914d77041', '34.139.115.110', '2022-05-08 08:27:13', 'false'),
('03df57cbac17526b749ae3ea5a590a70', '112.215.154.11', '2022-05-08 19:11:06', 'true'),
('743c4b28e239085f320b6e275cab9488', '112.215.154.11', '2022-05-08 19:11:07', 'true'),
('65db26c701012b5f9d423ff2b117094d', '112.215.154.11', '2022-05-08 19:11:11', 'true'),
('4a0e3dd9a20f71c72d5443f0ecf24727', '17.121.114.65', '2022-05-10 14:29:14', 'false'),
('70791a9d31822f914ba8d32bcbfbfb30', '17.121.113.253', '2022-05-10 23:32:57', 'true'),
('c02dec56f1983cea09d7cb08a5ff919a', '36.99.136.141', '2022-05-11 01:18:08', 'false'),
('99738f5aa55d50580f6618fd605fd1e5', '36.99.136.139', '2022-05-11 01:18:48', 'false'),
('37733dff07077ac4b7e1a177358869fa', '36.99.136.143', '2022-05-11 01:18:52', 'false'),
('e24d02a018a585b01ad39adfe2abd2b5', '127.0.0.1', '2022-05-11 10:00:04', 'false'),
('9eeefa23d1307e84a10c7311f6b00806', '127.0.0.1', '2022-05-11 10:03:28', 'false'),
('864252a8b755353a0b9b5b98015277f8', '127.0.0.1', '2022-05-11 10:06:33', 'false'),
('bc24fc474d730a5791b023a767cdda1b', '127.0.0.1', '2022-05-11 10:06:53', 'false'),
('2072f20a21c5599d2d543c4de58fb2a4', '127.0.0.1', '2022-05-11 15:40:54', 'true');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_jadwal`
--

CREATE TABLE `m_jadwal` (
  `kd` varchar(50) NOT NULL,
  `no` varchar(100) DEFAULT NULL,
  `durasi` varchar(100) DEFAULT NULL,
  `mapel` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `soal_jml` varchar(10) DEFAULT NULL,
  `soal_postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_jadwal`
--

INSERT INTO `m_jadwal` (`kd`, `no`, `durasi`, `mapel`, `postdate`, `soal_jml`, `soal_postdate`) VALUES
('86811b322a8e929b8eea798ede89457e', '3', '5', 'OTKP xstrix OTOMATISASI DAN TATA KELOLA PERKANTORAN', '2021-12-31 10:31:34', '5', '2022-01-06 14:51:24'),
('bfc87cf50cbb5a62d3e1ef961d854212', '2', '5', 'BPDPE xstrix Bisnis Daring dan Pemasaran', '2021-12-31 10:31:22', '5', '2022-01-06 14:51:09'),
('2c71ab8b8695560918004c724a6aa246', '1', '5', 'AKL xstrix Akuntansi dan Keuangan Lembaga', '2021-12-31 10:31:05', '5', '2022-01-06 14:50:37'),
('041056c1a252372f14dfb60a50e4ec72', '4', '5', 'RPL. Rekayasa Perangkat Lunak', '2021-12-31 10:30:10', '5', '2022-01-06 14:52:12'),
('42251bf8cd237ff9065eb289dd683103', '5', '5', 'TBO xstrix Tata Boga', '2021-12-31 10:31:51', '5', '2022-01-06 14:52:38'),
('6bc213d99845f7eaeba12abba80fa8e3', '6', '5', 'TBUS xstrix Tata Busana', '2021-12-31 10:32:05', '5', '2022-01-02 22:11:08'),
('375d075a0c9ba672746a725060d28809', '7', '5', 'TKRO xstrix Teknik Kendaraan Ringan Otomotif', '2021-12-31 10:32:16', '5', '2022-01-06 14:50:30'),
('f9d41bd1e1b89e4ec7d9695f74b0126f', '8', '5', 'TBSM xstrix Teknik dan Bisnis Sepeda Motor', '2022-01-06 13:54:52', '5', '2022-01-06 15:10:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_soal`
--

CREATE TABLE `m_soal` (
  `kd` varchar(50) NOT NULL,
  `jadwal_kd` varchar(50) DEFAULT NULL,
  `no` varchar(10) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `kunci` varchar(1) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `m_soal`
--

INSERT INTO `m_soal` (`kd`, `jadwal_kd`, `no`, `isi`, `kunci`, `postdate`) VALUES
('3e3377729e247fb804e4d8686344f84c', '55c95c485a333e54bd44ca716dcb1b94', '1', 'xkkirixpxkkananxsoal #1xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:55:41'),
('92093906534c53d8c07754b208be7ec3', '55c95c485a333e54bd44ca716dcb1b94', '2', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx', 'E', '2020-08-03 07:56:10'),
('23023360996716692f594f7cf275d8de', '55c95c485a333e54bd44ca716dcb1b94', '3', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx', 'E', '2020-08-03 07:56:26'),
('de9ac70e27d61e94173d36e8fa778d6e', '1bef0fe8e6c4ef1995620af41625a812', '1', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx', 'C', '2020-08-03 07:56:48'),
('8596e172b232bc79e5a4de3380a4d371', '1bef0fe8e6c4ef1995620af41625a812', '2', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:57:03'),
('45a140a41f3d896e065c2b75acb482ef', '869d3f414e0079c089dfc584b20a777b', '1', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:57:24'),
('7e1e69fc310d268a93879e806920a61f', '869d3f414e0079c089dfc584b20a777b', '2', 'xkkirixpxkkananxsoal..xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:57:39'),
('5f7450c1537369fa518132942d561f45', '1d431e0b88fb2563f810ea59141aa1ba', '1', 'xkkirixpxkkananxsoal..xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:58:05'),
('6700f9d76db5e803e8c82389a57b24d4', '1d431e0b88fb2563f810ea59141aa1ba', '2', 'xkkirixpxkkananxsoal..xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'C', '2020-08-03 07:58:21'),
('d059812b7bcb6d75b4247fe1b34cffa2', '83fb625a31b8f0f0020681c706c392b6', '1', 'xkkirixpxkkananxsoal..xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:58:47'),
('f985a2f582f5492d67e2c451485ee544', '83fb625a31b8f0f0020681c706c392b6', '2', 'xkkirixpxkkananxsoal..xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'B', '2020-08-03 07:59:05'),
('1c23fbaf02ff5de88442c9428d8e527b', '1bef0fe8e6c4ef1995620af41625a812', '3', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx', 'C', '2020-08-03 09:40:12'),
('f4138348ac58252457e4e4e2bdd62383', '55c95c485a333e54bd44ca716dcb1b94', '4', 'xkkirixpxkkananxsoal...xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE.xkkirixxgmringxpxkkananx', 'D', '2020-08-03 16:56:59'),
('68cf14d73f5f9f069aa83cec9766412e', '2c71ab8b8695560918004c724a6aa246', '1', 'xkkirixdivxkkananxDari pernyataan dibawah ini yang benar adalah &hellipxkommax.xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxa. Aktiva = kewajiban&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax d. Aktiva = ekuitas xstrix kewajibanxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxb. Aktiva = kewajiban &ndashxkommax ekuitas&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax e.xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxc. Aktiva = kewajiban + ekuitasxkkirixxgmringxdivxkkananx', 'A', '2022-01-02 21:36:38'),
('87d0e695e450e53d4e0c3df70fa43a4b', '2c71ab8b8695560918004c724a6aa246', '2', 'xkkirixdivxkkananxYang termasuk kedalam kelompok xkkirixuxkkananxaktivaxkkirixxgmringxuxkkananx adalah &hellipxkommax.xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxa.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax Kas, peralatan dan modal&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax c. kas, perlengkapan, peralatanxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxb.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax Perlengkapan peralatan, modal&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax d. peralatan, utang bank, modalxkkirixxgmringxdivxkkananx', 'A', '2022-01-02 21:37:29'),
('e6ab32d17e8bebb2df05a8a45c4aaa0b', '2c71ab8b8695560918004c724a6aa246', '3', 'xkkirixdivxkkananxHasil yang diperoleh dari kegiatan suatu usaha disebut &hellipxkommax.xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxa. Pendapatan&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax b. Beban&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxc. Prive&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax d. Keuntunganxkkirixxgmringxdivxkkananx', 'A', '2022-01-02 21:38:51'),
('bd4063db2ccf5e201144898e33af3660', '2c71ab8b8695560918004c724a6aa246', '4', 'xkkirixdivxkkananxJika jumlah pendapatan lebih besar dari beban maka akan terjadi &hellipxkommax.xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxa.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax Rugi bersih&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax b. Laba bersih&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxc. Labaxgmringx Rugi&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax d. Saldo rugixkkirixxgmringxdivxkkananx', 'B', '2022-01-02 21:39:30'),
('c4bc182f39f5e26be9a85b854179d1a6', '2c71ab8b8695560918004c724a6aa246', '5', 'xkkirixdivxkkananxJika laba bersih lebih kecil dari prive, maka ekuitas akan &hellipxkommax.xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxa.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax Bertambah&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax b. Berkurang&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxc. Tetap&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax &nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax d. Samaxkkirixxgmringxdivxkkananx', 'B', '2022-01-02 21:40:55'),
('6d12172b31524dc6fc1f5388b093d5f5', 'bfc87cf50cbb5a62d3e1ef961d854212', '1', 'xkkirixpxkkananxBerikut ini yang bukan merupakan contoh mesin pencarixgmringx search di internet yaitu &hellipxkommaxxkkirixbr xgmringxxkkananx\r\na. Lycosxkkirixbr xgmringxxkkananx\r\nb. Browserxkkirixbr xgmringxxkkananx\r\nc. Altavistaxkkirixbr xgmringxxkkananx\r\nd. Google searchxkkirixbr xgmringxxkkananx\r\ne. Yahoo searchxkkirixxgmringxpxkkananx', 'B', '2022-01-02 21:42:03'),
('ae86e276f318704ad8f5942dab05aabf', 'bfc87cf50cbb5a62d3e1ef961d854212', '2', 'xkkirixpxkkananxWebsite yang memberikan izin kepada pengunjung untuk menulis atau mengedit artikel yang sudah ada dalam website tersebut dengan berbagai peraturan tertentu yaitu website ...xkkirixbr xgmringxxkkananx\r\na. Commercexkkirixbr xgmringxxkkananx\r\nb. Wikixkkirixbr xgmringxxkkananx\r\nc. Forumxkkirixbr xgmringxxkkananx\r\nd. Portalxkkirixbr xgmringxxkkananx\r\ne. Personalxkkirixxgmringxpxkkananx', 'B', '2022-01-02 21:42:33'),
('21e4865e000669fe75a5912271a5a089', 'bfc87cf50cbb5a62d3e1ef961d854212', '3', 'xkkirixpxkkananxBerbagai kegiatan yang dilakukan dalam dunia blog dinamakan ...xkkirixbr xgmringxxkkananx\r\na. Photobloggingxkkirixbr xgmringxxkkananx\r\nb. Bloggerxkkirixbr xgmringxxkkananx\r\nc. Bloggingxkkirixbr xgmringxxkkananx\r\nd. Blogospherexkkirixbr xgmringxxkkananx\r\ne. Postingxkkirixxgmringxpxkkananx', 'C', '2022-01-02 21:43:07'),
('119c2900609340bc66dee2c1396e6bbb', 'bfc87cf50cbb5a62d3e1ef961d854212', '4', 'xkkirixpxkkananxFitur &ldquoxkommaxshopping cart&rdquoxkommax dalam web toko online bermanfaat bagi pengunjung untuk &hellipxkommaxxkkirixbr xgmringxxkkananx\r\na. Melakukan bloggingxkkirixbr xgmringxxkkananx\r\nb. Melakukan kunjunganxkkirixbr xgmringxxkkananx\r\nc. Melakukan browsingxkkirixbr xgmringxxkkananx\r\nd. Melakukan downloadxkkirixbr xgmringxxkkananx\r\ne. Melakukan pemesanan secara onlinexkkirixxgmringxpxkkananx', 'E', '2022-01-02 21:43:28'),
('80a71fbd0f5de6898c8474a380eced1e', 'bfc87cf50cbb5a62d3e1ef961d854212', '5', 'xkkirixpxkkananxBerikut ini yang tidak termasuk keuntungan atau kelebihan dari afiliasi yaitu&hellipxkommaxxkkirixbr xgmringxxkkananx\r\na. Menyita banyak waktuxkkirixbr xgmringxxkkananx\r\nb. Tanpa resiko kerugianxkkirixbr xgmringxxkkananx\r\nc. Peluang penghasilan yang besarxkkirixbr xgmringxxkkananx\r\nd. Sangat mudah dijalankanxkkirixbr xgmringxxkkananx\r\ne. Banyak pilihan dan celah peluangxkkirixxgmringxpxkkananx', 'A', '2022-01-02 21:43:51'),
('6842b0b8ce7d5825376e726e3acf3bbc', '86811b322a8e929b8eea798ede89457e', '1', 'xkkirixdivxkkananxSurat pribadi berikut yang sifatnya tidak sepenuhnya resmi adalah ...xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxA. Surat komplainxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxB. Surat lamaran pekerjaanxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxC. Surat persahabatanxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxD. Surat undanganxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxE. Surat untuk keluargaxkkirixxgmringxdivxkkananx', 'E', '2022-01-02 21:46:13'),
('124c9cbab7628b5026fb17635a58285f', '86811b322a8e929b8eea798ede89457e', '2', 'xkkirixdivxkkananx&nbspxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxPengunaan tanda garis miring pada surat berikut adalah ..xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxA. Nomor : R.xgmringxKopxgmringxIVxgmringx 2019xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxB. Nomor : Rxgmringx24xgmringxKopxgmringxIVxgmringx2019xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxC. Nomor : Rxgmringx24xstrixxgmringxKopxgmringxIVxgmringx2019xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxD. Nomor : Rxgmringx24xgmringxIVxgmringxKop.2019xgmringxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxE. Nomor : Rxstrix24xgmringxIVxgmringxKopxstrix2019xkkirixxgmringxdivxkkananx', 'B', '2022-01-02 21:47:21'),
('b69cfeedc9b34f69d9a678579c9c442c', '86811b322a8e929b8eea798ede89457e', '3', 'xkkirixdivxkkananxBapak Hidayat, seorang dosen,menduduki jabatan fungsional ahli utama. Batas usia personalia Bapak Hidayat adalah ...xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxA. 56 tahunxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxB. 58 tahunxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxC. 60 tahunxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxD. 65 tahunxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxE. 70 tahunxkkirixxgmringxdivxkkananx', 'B', '2022-01-02 21:48:38'),
('5fb617098c4a6465abe1091cb8a0c757', '86811b322a8e929b8eea798ede89457e', '4', 'xkkirixdivxkkananxBapak Sidik&nbspxkommax adalah seorang pegawai teladan yang memiliki dedikasi kerja yang baik dalam 1 tahun terakhir. Beliau berhak mendaptkan cuti tahunan selama ...xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxA. 3 harixkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxB. 6 harixkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxC. 10 harixkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxD. 12 harixkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxE. 15 harixkkirixxgmringxdivxkkananx', 'D', '2022-01-02 21:49:15'),
('3ecf0a77bb810003c140df4d6b4294b6', '86811b322a8e929b8eea798ede89457e', '5', 'xkkirixdivxkkananxKalimat berikut yang xkkirixuxkkananxbukan xkkirixxgmringxuxkkananxbagian dari surat promosi penjualan adalah ...​xkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxA. &ldquoxkommaxBila anda berbelanja sebelum tanggal 31 November, anda akan mendapatkan kalender cantik 2019&rdquoxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxB. &ldquoxkommaxkami yakin bahwa anda akan mempergunakan kesempatan yang istimewa ini&rdquoxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxC. &ldquoxkommaxuntuk pembelian di atas Rp1.000.000,00, anda akan mendapatkan potongan harga sebesar 5xpersenx&rdquoxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxD. &ldquoxkommaxbarang produksi kami akan memberikan manfaat yang besar bagi anda&rdquoxkommaxxkkirixxgmringxdivxkkananx\r\n\r\nxkkirixdivxkkananxE. &ldquoxkommaxberdasarkan surat saudara nomor 20 tentang pengaduan barang rusak....&rdquoxkommaxxkkirixxgmringxdivxkkananx', 'E', '2022-01-02 21:50:12'),
('58a0181f1bcb6b41ebf6c98f4d2de883', '041056c1a252372f14dfb60a50e4ec72', '1', 'xkkirixpxkkananxApa itu Software Engineering?xkkirixbr xgmringxxkkananx\r\na. Sebuah program komputer dan dokumentasi terkaitxkkirixbr xgmringxxkkananx\r\nb. Disiplin ilmu teknik yang berkaitan dengan semua aspek produksi perangkat lunak dari tahap awal spesifikasi sistem hingga pemeliharaan sistem setelah mulai digunakanxkkirixbr xgmringxxkkananx\r\nc. Semua aspek pengembangan sistem berbasis komputer termasuk perangkat keras, perangkat lunak dan rekayasa prosesxkkirixbr xgmringxxkkananx\r\nd. Orang yang memproduksi perangkat lunakxkkirixxgmringxpxkkananx', 'B', '2022-01-02 21:51:38'),
('e6670359a369da7fbcd69fc0d32f58b2', '041056c1a252372f14dfb60a50e4ec72', '2', 'xkkirixpxkkananxSistem aplikasi yang berjalan di komputer lokal, seperti PC dan tidak perlu terhubung ke jaringan, merupakan definisi dari tipe aplikasi:xkkirixbr xgmringxxkkananx\r\na. Entertainment systemsxkkirixbr xgmringxxkkananx\r\nb. Interactive transactionxstrixbased applicationsxkkirixbr xgmringxxkkananx\r\nc. Standxstrixalone applicationsxkkirixbr xgmringxxkkananx\r\nd. Embedded control systemsxkkirixxgmringxpxkkananx', 'C', '2022-01-02 21:52:06'),
('1841c7fd918016107f04f1e41c8b1c8a', '041056c1a252372f14dfb60a50e4ec72', '3', 'xkkirixpxkkananxPerangkat lunak yang baik adalah aplikasi yang memiliki daya tanggap, waktu pemrosesan serta pemanfaatan memori lebih sedikit, merupakan definisi dari atribut:xkkirixbr xgmringxxkkananx\r\na. Acceptabilityxkkirixbr xgmringxxkkananx\r\nb. Dependability and securityxkkirixbr xgmringxxkkananx\r\nc. Maintainabilityxkkirixbr xgmringxxkkananx\r\nd. Efficiencyxkkirixxgmringxpxkkananx', 'D', '2022-01-02 21:52:34'),
('27929aad4fc48377a1bad070d78d0000', '041056c1a252372f14dfb60a50e4ec72', '4', 'xkkirixpxkkananxProses perangkat lunak antara lain. kecuali:xkkirixbr xgmringxxkkananx\r\na. Maintenancexkkirixbr xgmringxxkkananx\r\nb. Validationxkkirixbr xgmringxxkkananx\r\nc. Design and implementationxkkirixbr xgmringxxkkananx\r\nd. Evolutionxkkirixxgmringxpxkkananx', 'A', '2022-01-02 21:53:09'),
('dcc549bfb0c0e19feefa065b6dec129e', '041056c1a252372f14dfb60a50e4ec72', '5', 'xkkirixpxkkananxMendesain struktur data sistem dan bagaimana sistem diwakili dalam basis data, merupakan proses desain:xkkirixbr xgmringxxkkananx\r\na. Component selection and designxkkirixbr xgmringxxkkananx\r\nb. Databasexkkirixbr xgmringxxkkananx\r\nc. Architecturalxkkirixbr xgmringxxkkananx\r\nd. Interfacexkkirixxgmringxpxkkananx', 'B', '2022-01-02 21:53:40'),
('b6ccb0aa34cd8498688e2b621aea2426', '42251bf8cd237ff9065eb289dd683103', '1', 'xkkirixpxkkananxBisnis usaha boga dimana makanan disiapkan, diolah, dan disajikan secara cepat adalah&hellipxkommaxxkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax&nbspxkommax&nbspxkommax cateringxkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax warung makanxkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax fast foodxkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax&nbspxkommax&nbspxkommax restoranxkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax kantinxkkirixxgmringxpxkkananx', 'C', '2022-01-02 21:55:01'),
('75e07aead382447cf1ac9f802dde6d91', '42251bf8cd237ff9065eb289dd683103', '2', 'xkkirixpxkkananxPromosi yang tepat untuk bisnis makanan adalah&hellipxkommaxxkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax&nbspxkommax&nbspxkommax mengutamakan kualitas rasa dan pelayananxkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax mengutamakan keuntunganxkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax mengutamakan kebaikan pengusahaxkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax&nbspxkommax&nbspxkommax mengutamakan kerugian pengusahaxkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax membagikan makanan gratis setiap harixkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananx&nbspxkommaxxkkirixxgmringxpxkkananx', 'A', '2022-01-02 21:55:28'),
('fba9a499ab62cf9451fbe8cdd6e67d90', '42251bf8cd237ff9065eb289dd683103', '3', 'xkkirixpxkkananxHalxstrixhal yang menyangkut pelaksanaan kegiatan pada bidang produksi yaitu sebagai berikut&hellipxkommaxxkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax&nbspxkommax&nbspxkommax bagian pengolahanxkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax bagian kebersihanxkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax bagian distribusixkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax&nbspxkommax&nbspxkommax bagian pelayananxkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax semua benarxkkirixxgmringxpxkkananx', 'E', '2022-01-02 21:56:35'),
('fad1f2b3f9342ba875477e0f791de0aa', '42251bf8cd237ff9065eb289dd683103', '4', 'xkkirixpxkkananxYang termasuk perencanaan pembiayaan pada catering adalah&hellipxkommax&hellipxkommax&hellipxkommax..xkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax&nbspxkommax&nbspxkommax biaya penyediaan bahan bakuxkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax biaya bahan jadixkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax biaya produksixkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax&nbspxkommax&nbspxkommax biaya bersihxkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax&nbspxkommax&nbspxkommax biaya listrikxkkirixxgmringxpxkkananx', 'C', '2022-01-02 21:57:29'),
('cc6a3f626417af20dc3d16de51c36a7f', '42251bf8cd237ff9065eb289dd683103', '5', 'xkkirixpxkkananxBahan cair yang digunakan untuk pembuatan sauce adalah&hellipxkommax&hellipxkommax&hellipxkommax&hellipxkommaxxkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax Stock, air, santanxkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax Stock, minyak, santanxkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax Stock, susu, tomato juice ,butter, minyakxkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax Air, susu, minyakxkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax Air, susu, margarinexkkirixxgmringxpxkkananx', 'C', '2022-01-02 21:58:10'),
('ed818d3a5d52279858f7383d1bf982d6', '6bc213d99845f7eaeba12abba80fa8e3', '1', 'xkkirixpxkkananxUntuk menjahit baju, membutuhkan :xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA. Pisauxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB. Mejaxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC. Jarumxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD. Karung Gonixkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE. Paluxkkirixxgmringxpxkkananx', 'C', '2022-01-02 22:03:59'),
('34280ae3a5dcf0dea5857a53b10d0ed4', '6bc213d99845f7eaeba12abba80fa8e3', '2', 'xkkirixpxkkananxSeorang Tukang Jahit, disebut juga dengan :xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA. Garmenxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB. Tailorxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC. Carpenterxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD. Lawyerxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE. Semua Benarxkkirixxgmringxpxkkananx', 'B', '2022-01-02 22:05:14'),
('62416e9523fb28a18080b39cd9e10b08', '6bc213d99845f7eaeba12abba80fa8e3', '3', 'xkkirixpxkkananxBahan untuk memberikan minyak pada mesin jahit yakni :xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA. Bensinxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB. Bensolxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC. Pelumasxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD. Olixkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE. Semua Benar.xkkirixxgmringxpxkkananx', 'C', '2022-01-02 22:07:00'),
('61d9d60bcff549a50625edfb06ce6815', '6bc213d99845f7eaeba12abba80fa8e3', '4', 'xkkirixpxkkananxUntuk pakaian dewasa, dengan tubuh langsing atau atletis, membutuhkan bahan sekitar :xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA. 1,5 Meterxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB. 2 Meterxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC. 1 Meterxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD. 3 Meterxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE. 4 Meterxkkirixxgmringxpxkkananx', 'A', '2022-01-02 22:08:53'),
('40455a412fbbc5543efb848ca8c318fe', '6bc213d99845f7eaeba12abba80fa8e3', '5', 'xkkirixpxkkananxPosisi kancing depan, sebagai cirikhas untuk baju pria, berada di sebelah :xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxA. Kananxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxB. Kirixkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxC. Bawahxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxD. Atasxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxE. Semua Benarxkkirixxgmringxpxkkananx', 'A', '2022-01-02 22:11:08'),
('51b837f5a8ac442bf4e34eea3705c8d5', '375d075a0c9ba672746a725060d28809', '1', 'xkkirixpxkkananxSuatu kombinasi dari setiap tindakan yang dilakukan untuk menjaga suatu barang dalam, atau untuk memperbaikinya sampai suatu kondisi yang bisa diterima merupakan pengertian dari &hellipxkommax.xkkirixbr xgmringxxkkananx\r\nA. Perawatanxkkirixbr xgmringxxkkananx\r\nB. Pemeliharaanxkkirixbr xgmringxxkkananx\r\nC. Pengecekanxkkirixbr xgmringxxkkananx\r\nD. Pengurasanxkkirixbr xgmringxxkkananx\r\nE. Enginexkkirixxgmringxpxkkananx', 'B', '2022-01-02 22:18:52'),
('90e82699c915390753e5396ef4891bc3', '375d075a0c9ba672746a725060d28809', '2', 'xkkirixpxkkananxPemeliharaan meliputi &hellipxkommax.xkkirixbr xgmringxxkkananx\r\nA. Perawatanxkkirixbr xgmringxxkkananx\r\nB. Perbaikanxkkirixbr xgmringxxkkananx\r\nC. Perawatan dan perbaikanxkkirixbr xgmringxxkkananx\r\nD. Pengecekanxkkirixbr xgmringxxkkananx\r\nE. Pemeliharaanxkkirixxgmringxpxkkananx', 'C', '2022-01-02 22:19:15'),
('8d7da5bba2ffceeac18990f25cea0b43', '375d075a0c9ba672746a725060d28809', '3', 'xkkirixpxkkananxJika sewaktuxstrixwaktu terjadi kerusakan di luar jadwal perawatan berkala hal ini disebut &hellipxkommax.xkkirixbr xgmringxxkkananx\r\nA. Perbaikanxkkirixbr xgmringxxkkananx\r\nB. Perawatan tak terencanaxkkirixbr xgmringxxkkananx\r\nC. Perawatan sabuk timingxkkirixbr xgmringxxkkananx\r\nD. Perawatan terencanaxkkirixbr xgmringxxkkananx\r\nE. Penyetelanxkkirixxgmringxpxkkananx', 'B', '2022-01-02 22:19:40'),
('2dd8c7036840d6a37e86990a8c86eb15', '375d075a0c9ba672746a725060d28809', '4', 'xkkirixpxkkananxMerupakan perawatan pertama yang mengganti komponen dilakukan saat kendaraan telah mencapai berapa km &hellipxkommax.xkkirixbr xgmringxxkkananx\r\nA. 1.000 kmxkkirixbr xgmringxxkkananx\r\nB. 10.000 kmxkkirixbr xgmringxxkkananx\r\nC. 20.000 kmxkkirixbr xgmringxxkkananx\r\nD. 30.000 kmxkkirixbr xgmringxxkkananx\r\nE. 40.000 kmxkkirixxgmringxpxkkananx', 'B', '2022-01-02 22:21:15'),
('8d49d9aca80fc89edd8d674f83ba8cfe', '375d075a0c9ba672746a725060d28809', '5', 'xkkirixpxkkananxBerapakah derajat temperatur mesin panas &hellipxkommax.xkkirixbr xgmringxxkkananx\r\nA. 50&degxkommaxCxkkirixbr xgmringxxkkananx\r\nB. 40&degxkommaxCxkkirixbr xgmringxxkkananx\r\nC. 90&degxkommaxCxkkirixbr xgmringxxkkananx\r\nD. 70&degxkommaxCxkkirixbr xgmringxxkkananx\r\nE. 80&degxkommaxCxkkirixxgmringxpxkkananx', 'E', '2022-01-02 22:21:46'),
('ff839d9031202c71a6d9acb716df8df1', 'f9d41bd1e1b89e4ec7d9695f74b0126f', '1', 'xkkirixpxkkananxYang termasuk fungsi batrey adalah...&nbspxkommaxxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxa. Sumber arus sementaraxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxb. Sebagai penggerak roda gila xgmringx fly wellxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxc. Kuat arusxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxd. Teganganxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxe. Sumber energixkkirixxgmringxpxkkananx', 'A', '2022-01-06 14:57:25'),
('abd08041b01b1b09011aa9d965ca190e', 'f9d41bd1e1b89e4ec7d9695f74b0126f', '2', 'xkkirixpxkkananxSebuah sepeda motor mengalami masalah tidak mau hidup, kemungkinan penyebab gangguan tersebut yang paling tepat adalah....xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxa. Pilot air jet tersumbatxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxb. Pipa bahan bakar tersumbatxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxc. Needle valve ausxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxd. Spring pada needle valve patahxkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxe. Pelampung tidak bekerja optimalxkkirixxgmringxpxkkananx', 'B', '2022-01-06 14:58:34'),
('8e7e2721cf1a7ea5a0563b5c82b823d7', 'f9d41bd1e1b89e4ec7d9695f74b0126f', '3', 'xkkirixpxkkananxLangkah keselamatan kerja yang tepat untuk menanggulangi gas CO hasil sisaxkkirixbr xgmringxxkkananx\r\nPembakaran di bengkel sepeda motor adalah :xkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax&nbspxkommax &nbspxkommaxMembuat instalasi gas&nbspxkommax buang yang memadai.xkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax &nbspxkommaxPara pekerja menggunakan masker.xkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax &nbspxkommaxBahan &ndashxkommax bahan kimia di tempatkan khusus.xkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax&nbspxkommax &nbspxkommaxBarang &ndashxkommax barang mudah terbakar di jauhkan dari sumber api.xkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax &nbspxkommaxDibuatkan tempat pengolahan limbah sisa hasil pembakaran.xkkirixxgmringxpxkkananx', 'A', '2022-01-06 15:04:23'),
('d761d46e23f85bb50e6beeeeda297b87', 'f9d41bd1e1b89e4ec7d9695f74b0126f', '4', 'xkkirixpxkkananxTujuan keselamatan dan kesehatan kerja adalah :xkkirixbr xgmringxxkkananx\r\nA.&nbspxkommax&nbspxkommax &nbspxkommaxMencegah terjadinya kecelakaan kerja.xkkirixbr xgmringxxkkananx\r\nB.&nbspxkommax&nbspxkommax &nbspxkommaxMencegah timbulnya suatu penyakit akibat suatu pekerjaan.xkkirixbr xgmringxxkkananx\r\nC.&nbspxkommax&nbspxkommax &nbspxkommaxMencegah atau mengurangi cacat mental atau cacat tetap.xkkirixbr xgmringxxkkananx\r\nD.&nbspxkommax&nbspxkommax &nbspxkommaxMencegah pemborosan tenaga kerja.xkkirixbr xgmringxxkkananx\r\nE.&nbspxkommax&nbspxkommax &nbspxkommaxJawaban Salah Semuaxkkirixxgmringxpxkkananx', 'E', '2022-01-06 15:06:21'),
('dadd0598d82505066b14c15dcb237906', 'f9d41bd1e1b89e4ec7d9695f74b0126f', '5', 'xkkirixpxkkananxApakah kamu mengetahui tentang komponen system pengapian yang sangat penting di antara nya adalah Busi, Busi berfungsi yang sangat vital di kendaraan. Tahukah kamu Berapakah harga Busi pada sepeda motor ...xkkirixbr xgmringxxkkananx\r\nA. 15.000xkkirixbr xgmringxxkkananx\r\nB. 50.000xkkirixbr xgmringxxkkananx\r\nC. 75.000xkkirixbr xgmringxxkkananx\r\nD. 100.000xkkirixbr xgmringxxkkananx\r\nE. 200.000xkkirixxgmringxpxkkananx', 'A', '2022-01-06 15:10:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_soal_filebox`
--

CREATE TABLE `m_soal_filebox` (
  `kd` varchar(50) NOT NULL,
  `soal_kd` varchar(50) DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_tapel`
--

CREATE TABLE `m_tapel` (
  `kd` varchar(50) NOT NULL,
  `tahun1` varchar(4) DEFAULT NULL,
  `tahun2` varchar(4) DEFAULT NULL,
  `status` enum('true','false') NOT NULL DEFAULT 'false',
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_tapel`
--

INSERT INTO `m_tapel` (`kd`, `tahun1`, `tahun2`, `status`, `postdate`) VALUES
('1521f139ca3892fb5afff72a15df510a', '2022', '2023', 'true', '2020-08-03 07:29:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nomer2022`
--

CREATE TABLE `nomer2022` (
  `noid` int(50) NOT NULL,
  `calon_kd` varchar(50) DEFAULT NULL,
  `calon_noreg` varchar(100) DEFAULT NULL,
  `calon_nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nomer2022`
--

INSERT INTO `nomer2022` (`noid`, `calon_kd`, `calon_noreg`, `calon_nama`, `postdate`) VALUES
(12, '1249dfa06037d227712a1d953bf83d03', '0012', '0', '2022-01-06 10:33:52'),
(11, 'b07f2c2f9bbb64f11c47c584c3ebe7c4', '0011', '1', '2022-01-06 10:31:57'),
(10, 'b07f2c2f9bbb64f11c47c584c3ebe7c4', '0011', '1', '2022-01-06 10:31:57'),
(9, '9170b2a7bb8be06430cb4e244d049c73', '0009', '1', '2022-01-06 10:30:43'),
(13, '3beb3b56a1f6771967500e3f514c2bcd', '0013', '0', '2022-01-06 10:34:28'),
(14, '0489036220db36eef8154f0d70b03a81', '0014', '9', '2022-01-06 10:35:31'),
(15, '6dc20b58e5feac9f5868b6d40adb4bb0', '0015', '9', '2022-01-06 10:36:28'),
(16, '1a267bd31d3cb1e089f505c820e47249', '0016', '9', '2022-01-06 10:38:11'),
(17, '3a76ba863910395e0fef3b3635f5d970', '0017', '9', '2022-01-06 10:38:51'),
(18, 'ea735c404a1fc5d0e3d8d9b2e9ddac7f', '0018', '9', '2022-01-06 10:39:15'),
(19, 'cb549aeeed3092fc2b0fd1d6b52db698', '0019', '9', '2022-01-06 10:39:46'),
(20, '18cb072a4e0b00e37321474c95f58911', '0020', '9', '2022-01-06 10:40:21'),
(21, '8ef055edca2ad2f9e75917bd9ece25c5', '0021', '9', '2022-01-06 10:40:50'),
(22, '6d47f6a43d5574ffd80cf86f910d72ed', '0022', '8', '2022-01-06 10:47:21'),
(23, '3333034369d5d59e04d0a0e6d45503cd', '0023', '8', '2022-01-06 10:48:00'),
(24, 'aab2399fd6afa8e8cebf02bfa3f36038', '0024', '8', '2022-01-06 10:49:01'),
(25, '75f0b7d3f0d226982572a5747a94738d', '0025', '5', '2022-01-06 10:57:22'),
(26, '77054989cf2e1784d898febace241a25', '0026', '5', '2022-01-06 11:02:35'),
(27, 'cbbcfb4724561d26602318c9c17c5a69', '0027', '5', '2022-01-06 11:03:43'),
(28, '31054d259294085025df7f7effc4c556', '0028', '5', '2022-01-06 11:04:58'),
(29, '032487be75109d51f535c4bb737bd4de', '0029', '1', '2022-01-06 11:12:16'),
(30, '217b209efa3356ddbb3e1d19f0bd7b9b', '0030', '9', '2022-01-06 11:21:12'),
(31, '73f032b81cafdaae6c1ec63c7356bd78', '0031', '8', '2022-01-06 13:37:10'),
(32, '0425202d47b6594106a0053b67cef0df', '0032', '8', '2022-01-06 13:37:25'),
(33, '67f2beea828f21804420920bed7a97e2', '0033', '11', '2022-01-06 13:42:42'),
(34, 'ff58941512437bd081c6b7c70655ec32', '0034', '11', '2022-01-06 13:42:54'),
(35, '58b46c2bc58d93578137bb35dc4a1597', '0035', '9', '2022-01-06 13:43:51'),
(36, '0050852fd1470820a1a25c6cb72b03f1', '0036', '1', '2022-01-06 13:57:16'),
(37, 'fd15f41b6df6d7bdb834780ed28704e5', '0037', 'UNTUNG YAINODIN', '2022-01-06 14:04:50'),
(38, 'fd424e5f7bcd13fc6add739b542c51a5', '0038', '1222', '2022-01-11 09:20:27'),
(39, '0f88169adf69401f2c90d9f604179224', '0039', 'j', '2022-01-27 10:43:10'),
(40, '0e973314478447a29ab0a666f42f232e', '0040', 'Dwi Lestari', '2022-01-31 13:41:55'),
(41, '05d3c6b5350d10b23b611bb147d6d6c5', '0041', 'M safiyan najikh', '2022-04-26 18:31:00'),
(42, 'e24d02a018a585b01ad39adfe2abd2b5', '0042', 'coba1', '2022-05-11 10:01:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `kd` varchar(50) NOT NULL,
  `tapel` varchar(100) DEFAULT NULL,
  `noreg` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `usernamex` varchar(50) DEFAULT NULL,
  `passwordx` varchar(50) DEFAULT NULL,
  `passwordx2` varchar(100) DEFAULT NULL,
  `kelamin` varchar(100) DEFAULT NULL,
  `lahir_tmp` varchar(100) DEFAULT NULL,
  `lahir_tgl` date DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `alamat_rt` varchar(50) DEFAULT NULL,
  `alamat_rw` varchar(50) DEFAULT NULL,
  `alamat_desa` varchar(100) DEFAULT NULL,
  `alamat_kec` varchar(100) DEFAULT NULL,
  `alamat_kab` varchar(100) DEFAULT NULL,
  `nohp` varchar(100) DEFAULT NULL,
  `sekolah_asal` varchar(100) DEFAULT NULL,
  `prestasi` varchar(100) DEFAULT NULL,
  `cek_kk` varchar(100) DEFAULT NULL,
  `cek_foto` varchar(100) DEFAULT NULL,
  `cek_ijazah` varchar(100) DEFAULT NULL,
  `cek_kip` varchar(100) DEFAULT NULL,
  `cek_pkh` varchar(100) DEFAULT NULL,
  `hasil_nilai` varchar(100) DEFAULT NULL,
  `hasil_jurusan` varchar(100) DEFAULT NULL,
  `hasil_jurusan2` varchar(100) DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`kd`, `tapel`, `noreg`, `nama`, `postdate`, `usernamex`, `passwordx`, `passwordx2`, `kelamin`, `lahir_tmp`, `lahir_tgl`, `alamat`, `alamat_rt`, `alamat_rw`, `alamat_desa`, `alamat_kec`, `alamat_kab`, `nohp`, `sekolah_asal`, `prestasi`, `cek_kk`, `cek_foto`, `cek_ijazah`, `cek_kip`, `cek_pkh`, `hasil_nilai`, `hasil_jurusan`, `hasil_jurusan2`, `dibaca`, `dibaca_postdate`) VALUES
('8ef055edca2ad2f9e75917bd9ece25c5', '2022xgmringx2023', '0021', '9', '2022-01-06 10:40:50', '0021', '331e92bf36b0c2a110e79afc8ee9197f', '002150', 'Perempuan', '12', '2022-01-19', '9', '812', '1', '2', '3', '1', '12', '12', '122', NULL, NULL, NULL, NULL, NULL, '20', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('6d47f6a43d5574ffd80cf86f910d72ed', '2022xgmringx2023', '0022', '8', '2022-01-06 10:47:21', '0022', '200957701c79f57ac6b78b2775b148fd', '002221', 'Laki xstrix Laki', '7', '2022-01-09', '7', '7', '7', '7', '7', '7', '7', '7', '7', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('3333034369d5d59e04d0a0e6d45503cd', '2022xgmringx2023', '0023', '8', '2022-01-06 10:48:00', '0023', '435bb69e7ccec68f9e3fb65ce1e23c7d', '002300', 'Laki xstrix Laki', '7', '2022-01-17', '7', '7', '7', '7', '7', '7', '7', '7', '7', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('aab2399fd6afa8e8cebf02bfa3f36038', '2022xgmringx2023', '0024', '8', '2022-01-06 10:49:01', '0024', '8af30b3b42df4f4ac4388d787bdebec2', '002401', 'Laki xstrix Laki', '7', '2022-01-27', '7', '7', '7', '7', '7', '7', '7', '7', '7', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('75f0b7d3f0d226982572a5747a94738d', '2022xgmringx2023', '0025', '5', '2022-01-06 10:57:22', '0025', 'cc2d93fafeded72d5962318bae5c1453', '002522', 'Laki xstrix Laki', '5', '2022-01-17', '4', '7', '6', '5', '6', '7', '23', '2', '233', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('77054989cf2e1784d898febace241a25', '2022xgmringx2023', '0026', '5', '2022-01-06 11:02:35', '0026', '365ccaa67a34fb4a7464f57d40450834', '002635', 'Laki xstrix Laki', '5', '2022-01-28', '4', '7', '6', '5', '6', '7', '23', '2', '233', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('cbbcfb4724561d26602318c9c17c5a69', '2022xgmringx2023', '0027', '5', '2022-01-06 11:03:43', '0027', 'b2adeb3f0999d60a5cc48c0d7555b0bc', '002743', 'Laki xstrix Laki', '5', '2022-01-29', '4', '7', '6', '5', '6', '7', '23', '2', '233', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('31054d259294085025df7f7effc4c556', '2022xgmringx2023', '0028', '5', '2022-01-06 11:04:58', '0028', '9b3a097dc4b2999e3e3a2d14fbf9be4f', '002858', 'Laki xstrix Laki', '5', '2022-01-27', '4', '7', '6', '5', '6', '7', '23', '2', '233', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('032487be75109d51f535c4bb737bd4de', '2022xgmringx2023', '0029', '1', '2022-01-06 11:12:16', '0029', 'ddf9ac037f4883df27af596b6eabe9dc', '002916', 'Laki xstrix Laki', '3', '2022-01-05', '3', '1', '2', '1', '12', '12', '12', '122', '223333', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('217b209efa3356ddbb3e1d19f0bd7b9b', '2022xgmringx2023', '0030', '9', '2022-01-06 11:21:12', '0030', 'ee93ff16cd09b9ae0a98f754af6224a0', '003012', 'Laki xstrix Laki', '233', '2022-01-04', '8', '7', '6', '5', '1', '2', '3', '1', '1', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('73f032b81cafdaae6c1ec63c7356bd78', '2022xgmringx2023', '0031', '8', '2022-01-06 13:37:10', '0031', '75fed3c886aa3153c0b4052b555aaa22', '003110', 'Laki xstrix Laki', '8', '2022-01-18', '8', '7', '3', '9', '8', '9', '8', '7', '6', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('0425202d47b6594106a0053b67cef0df', '2022xgmringx2023', '0032', '8', '2022-01-06 13:37:25', '0032', '924397e4d5c22e56a1fcf4c59575088c', '003225', 'Laki xstrix Laki', '8', '2022-01-02', '8', '7', '3', '9', '8', '9', '8', '7', '6', NULL, NULL, NULL, NULL, NULL, '20', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('67f2beea828f21804420920bed7a97e2', '2022xgmringx2023', '0033', '11', '2022-01-06 13:42:42', '0033', '95ce59ccf1c6aa86c79be33f513e3c6b', '003342', 'Laki xstrix Laki', '11', '2022-01-03', '11', '11', '11', '11', '11', '11', '11', '11', '11', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('ff58941512437bd081c6b7c70655ec32', '2022xgmringx2023', '0034', '11', '2022-01-06 13:42:54', '0034', '9ba4646fd542fcf82b35da9d6911bcc5', '003454', 'Laki xstrix Laki', '11', '2022-01-19', '11', '11', '11', '11', '11', '11', '11', '11', '11', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('58b46c2bc58d93578137bb35dc4a1597', '2022xgmringx2023', '0035', '9', '2022-01-06 13:43:51', '0035', '9774e763f465e75a1e9a5cb3f3cdc894', '003551', 'Laki xstrix Laki', '9', '2022-01-05', '9', '9', '9', '9', '9', '9', '9', '9', '9', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('0050852fd1470820a1a25c6cb72b03f1', '2022xgmringx2023', '0036', '1', '2022-01-06 13:57:16', '0036', '1fb66dd997843626820156036f5f8c97', '003616', 'Laki xstrix Laki', '2', '2022-01-24', '4', '2', '7', '8', '12', '9', '8', '19', '233', NULL, NULL, NULL, NULL, NULL, 'NAN', NULL, NULL, 'true', '2022-05-11 16:10:29'),
('fd424e5f7bcd13fc6add739b542c51a5', '2022xgmringx2023', '0038', '1222', '2022-01-11 09:20:27', '0038', 'e4688a8be9563fc260e0d048d625c899', '003827', 'Laki xstrix Laki', '122', '2022-01-12', '122', '1', '2', '122', '11', '1222', '1112', '222', '12333', 'ADA', 'ADA', 'ADA', 'ADA', 'ADA', '90', 'BPDPE xstrix Bisnis Daring dan Pemasaran', 'AKL xstrix Akuntansi dan Keuangan Lembaga', 'true', '2022-05-11 16:10:29'),
('0f88169adf69401f2c90d9f604179224', '2022xgmringx2023', '0039', 'j', '2022-01-27 10:43:10', '0039', '8fe9bb2cf2d6ff15217d3d5b6b8dcc55', '003910', 'Laki xstrix Laki', 'n', '2022-01-27', 'n', 'n', 'j', 'j', 'n', 'n', 'j', 'j', 'j', NULL, NULL, NULL, NULL, NULL, 'NAN', 'AKL xstrix Akuntansi dan Keuangan Lembaga', NULL, 'true', '2022-05-11 16:10:29'),
('0e973314478447a29ab0a666f42f232e', '2022xgmringx2023', '0040', 'Dwi Lestari', '2022-01-31 13:41:55', '0040', '0b33ec76b56d9ba9c3f7ff3fb0c7ffee', '004055', 'Perempuan', 'Pekalongan', '2007-05-28', 'Karanganyar Tirto pekalongan', '05', '03', 'Karanganyar', 'Tirto', 'Pekalongan', '+62 858xstrix7614xstrix1855', 'MTs Salafiyah NU Karanganyar', 'Pramuka Garuda', NULL, NULL, NULL, NULL, NULL, 'NAN', 'RPL. Rekayasa Perangkat Lunak', NULL, 'true', '2022-05-11 16:10:29'),
('05d3c6b5350d10b23b611bb147d6d6c5', '2022xgmringx2023', '0041', 'M safiyan najikh', '2022-04-26 18:31:00', '0041', '37bd0db6bad46105960b5b6e9722b955', '004100', 'Laki xstrix Laki', 'Kendal', '2007-02-19', 'Desa krompaan kec gemuh kendal', '04', '04', 'Krompaan', 'Gemuh', 'Kendal', '083842818014', 'SMP NU 05 awwalul hidayah', 'Tidak ada', NULL, NULL, NULL, NULL, NULL, 'NAN', 'TBSM xstrix Teknik dan Bisnis Sepeda Motor', NULL, 'true', '2022-05-11 16:10:29'),
('e24d02a018a585b01ad39adfe2abd2b5', '2022xgmringx2023', '0042', 'coba1', '2022-05-11 10:01:36', '0042', '2fbdd1124fcd22b7b7229b42d970abf9', '004236', 'Laki xstrix Laki', 'kendal', '2022-05-11', 'jl. raya...', '01', '04', 'desa1', 'kec1', 'kendal', '081234567890', 'sekolah1', 'prestasi1', NULL, NULL, NULL, NULL, NULL, '40', 'BPDPE xstrix Bisnis Daring dan Pemasaran', NULL, 'true', '2022-05-11 16:10:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa_rekap`
--

CREATE TABLE `siswa_rekap` (
  `kd` varchar(50) NOT NULL,
  `siswa_kd` varchar(50) DEFAULT NULL,
  `siswa_noreg` varchar(100) DEFAULT NULL,
  `siswa_nama` varchar(100) DEFAULT NULL,
  `mapel` varchar(100) DEFAULT NULL,
  `skor` varchar(50) DEFAULT NULL,
  `diterima` enum('true','false') DEFAULT 'false',
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa_soal`
--

CREATE TABLE `siswa_soal` (
  `kd` varchar(50) NOT NULL,
  `jadwal_kd` varchar(50) DEFAULT NULL,
  `siswa_kd` varchar(50) DEFAULT NULL,
  `soal_kd` varchar(50) DEFAULT NULL,
  `jawab` varchar(1) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `kunci` varchar(1) DEFAULT NULL,
  `benar` enum('true','false') DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `siswa_soal`
--

INSERT INTO `siswa_soal` (`kd`, `jadwal_kd`, `siswa_kd`, `soal_kd`, `jawab`, `postdate`, `kunci`, `benar`) VALUES
('e9a4a1ba64c94290013afbaf5a018801', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', 'c4bc182f39f5e26be9a85b854179d1a6', 'A', '2022-01-06 11:33:50', 'B', 'false'),
('fd8333233f004a62ddfd857dd2615d6a', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', 'bd4063db2ccf5e201144898e33af3660', 'D', '2022-01-06 11:34:21', 'B', 'false'),
('cd4059a36752e82db4d5874d999b54bd', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', 'e6ab32d17e8bebb2df05a8a45c4aaa0b', 'A', '2022-01-06 11:34:15', 'A', 'true'),
('12c708b2edd66034b1c60de2ca0b0bb7', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '87d0e695e450e53d4e0c3df70fa43a4b', 'D', '2022-01-06 11:34:10', 'A', 'false'),
('08d679a05eb04aec76485a1447d89eb3', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '68cf14d73f5f9f069aa83cec9766412e', 'E', '2022-01-06 11:34:25', 'A', 'false'),
('1aa2e7a83ce85efc1dbd2df50a7281b5', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '80a71fbd0f5de6898c8474a380eced1e', 'B', '2022-01-06 11:35:35', 'A', 'false'),
('c387ec9025e624e58249eecf758f96c1', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '119c2900609340bc66dee2c1396e6bbb', 'B', '2022-01-06 11:35:29', 'E', 'false'),
('4544cd2049c4848423089c95b4601936', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '21e4865e000669fe75a5912271a5a089', 'D', '2022-01-06 11:35:32', 'C', 'false'),
('d8b924bb5574652ca6079d3e7ba78ad9', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', 'ae86e276f318704ad8f5942dab05aabf', 'E', '2022-01-06 11:35:38', 'B', 'false'),
('8b9a4057c502029acc93557457d1e995', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '6d12172b31524dc6fc1f5388b093d5f5', 'B', '2022-01-06 11:35:42', 'B', 'true'),
('3c6c5e0be14eb835e3cabce1a64021a2', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '40455a412fbbc5543efb848ca8c318fe', 'A', '2022-01-06 11:51:13', 'A', 'true'),
('60318d4962c4e4320a04fce13ca46072', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '61d9d60bcff549a50625edfb06ce6815', 'C', '2022-01-06 11:51:05', 'A', 'false'),
('d39f2afca25efaaefe24b7548750b595', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '62416e9523fb28a18080b39cd9e10b08', 'E', '2022-01-06 11:51:08', 'C', 'false'),
('0f9c5f249c3957fc307dd99f97f835c9', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '34280ae3a5dcf0dea5857a53b10d0ed4', 'A', '2022-01-06 11:50:53', 'B', 'false'),
('8b9d8aee65c81fbfb8a5ded85b4b197e', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', 'ed818d3a5d52279858f7383d1bf982d6', 'E', '2022-01-06 11:51:02', 'C', 'false'),
('781cf11b5948e601755fcef401f721be', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '8d49d9aca80fc89edd8d674f83ba8cfe', 'C', '2022-01-06 11:51:56', 'E', 'false'),
('9268f4a3bf8e57293ce24017e527eae1', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '2dd8c7036840d6a37e86990a8c86eb15', 'E', '2022-01-06 11:52:02', 'B', 'false'),
('cd7d9eb03c2011d514fc2cc283b700c2', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '8d7da5bba2ffceeac18990f25cea0b43', 'A', '2022-01-06 11:51:59', 'B', 'false'),
('7190e08eae857100256392b7062d7841', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '90e82699c915390753e5396ef4891bc3', 'B', '2022-01-06 11:52:04', 'C', 'false'),
('266471b2af8f552e27ea5bcd27a1ac50', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', '51b837f5a8ac442bf4e34eea3705c8d5', 'A', '2022-01-06 11:51:53', 'B', 'false'),
('3493994ad04bd30a6ba1fc6ba072b371', '2c71ab8b8695560918004c724a6aa246', '0425202d47b6594106a0053b67cef0df', 'c4bc182f39f5e26be9a85b854179d1a6', 'C', '2022-01-06 13:38:20', 'B', 'false'),
('cf61281323c40c53ed6f3527011da1a2', '2c71ab8b8695560918004c724a6aa246', '0425202d47b6594106a0053b67cef0df', 'bd4063db2ccf5e201144898e33af3660', 'A', '2022-01-06 13:38:00', 'B', 'false'),
('7c08a1c7e1990899ea89e7be33dea204', '2c71ab8b8695560918004c724a6aa246', '0425202d47b6594106a0053b67cef0df', 'e6ab32d17e8bebb2df05a8a45c4aaa0b', 'A', '2022-01-06 13:38:17', 'A', 'true'),
('22cef7facea378be2898675145f90617', '2c71ab8b8695560918004c724a6aa246', '0425202d47b6594106a0053b67cef0df', '87d0e695e450e53d4e0c3df70fa43a4b', 'B', '2022-01-06 13:38:13', 'A', 'false'),
('64e7b39af280957aff33045e188ce01d', '2c71ab8b8695560918004c724a6aa246', '0425202d47b6594106a0053b67cef0df', '68cf14d73f5f9f069aa83cec9766412e', 'C', '2022-01-06 13:38:07', 'A', 'false'),
('32abc6470d7ae2a4f4259d824696d3c1', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', 'c4bc182f39f5e26be9a85b854179d1a6', 'E', '2022-05-11 10:02:29', 'B', 'false'),
('11c3ceae2833512c16c4d69260be9c90', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', 'bd4063db2ccf5e201144898e33af3660', 'C', '2022-05-11 10:02:35', 'B', 'false'),
('0e82796f860f32a5b5851c8adcdcb5e0', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', 'e6ab32d17e8bebb2df05a8a45c4aaa0b', 'A', '2022-05-11 10:02:22', 'A', 'true'),
('932adc84c308b9e4698a609842fe7dc5', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '87d0e695e450e53d4e0c3df70fa43a4b', 'C', '2022-05-11 10:02:26', 'A', 'false'),
('3a0a00ff6ceeb5d7ec8c00a2e6c6dc8f', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '68cf14d73f5f9f069aa83cec9766412e', 'A', '2022-05-11 10:02:32', 'A', 'true'),
('2bcf5f86af7c1955b1bc199e56834fb6', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', 'dcc549bfb0c0e19feefa065b6dec129e', 'A', '2022-05-11 10:03:04', 'B', 'false'),
('9c90db605e834fc44ac63046f758db08', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '27929aad4fc48377a1bad070d78d0000', 'B', '2022-05-11 10:03:14', 'A', 'false'),
('5fa970c5a60cb8b76300deae5db8871f', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '1841c7fd918016107f04f1e41c8b1c8a', 'D', '2022-05-11 10:03:17', 'D', 'true'),
('144e41adf0b70edb0c03159d8b0255ea', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', 'e6670359a369da7fbcd69fc0d32f58b2', 'D', '2022-05-11 10:03:07', 'C', 'false'),
('89a0f642b1e2305bd55521811a6c6b00', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '58a0181f1bcb6b41ebf6c98f4d2de883', 'E', '2022-05-11 10:03:11', 'B', 'false'),
('5b0617c31149176f6eb4f96d7bdb6bed', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '8d49d9aca80fc89edd8d674f83ba8cfe', 'A', '2022-05-11 15:41:31', 'E', 'false'),
('025730023d494bbcc77fc8e33767bc6196', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '2dd8c7036840d6a37e86990a8c86eb15', '', '2022-05-11 15:41:27', 'B', 'false'),
('f959556f0df0d0f42b7e355fcf2b0d8357', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '8d7da5bba2ffceeac18990f25cea0b43', '', '2022-05-11 15:41:27', 'B', 'false'),
('a4dcc1e9c8bf4a479c320a42a9c769e342', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '90e82699c915390753e5396ef4891bc3', '', '2022-05-11 15:41:27', 'C', 'false'),
('b39b4f945252f29b82aa47e6f7af0f9e19', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', '51b837f5a8ac442bf4e34eea3705c8d5', '', '2022-05-11 15:41:27', 'B', 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa_soal_nilai`
--

CREATE TABLE `siswa_soal_nilai` (
  `kd` varchar(50) NOT NULL,
  `jadwal_kd` varchar(50) DEFAULT NULL,
  `siswa_kd` varchar(50) DEFAULT NULL,
  `siswa_noreg` varchar(100) DEFAULT NULL,
  `siswa_nama` varchar(100) DEFAULT NULL,
  `jml_benar` varchar(3) DEFAULT NULL,
  `jml_salah` varchar(3) DEFAULT NULL,
  `waktu_mulai` datetime DEFAULT NULL,
  `waktu_proses` datetime DEFAULT NULL,
  `waktu_akhir` datetime DEFAULT NULL,
  `skor` varchar(5) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `waktu_selesai` datetime DEFAULT NULL,
  `jml_soal_dikerjakan` varchar(10) DEFAULT NULL,
  `revisi_postdate` datetime DEFAULT NULL,
  `revisi` enum('true','false') NOT NULL DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `siswa_soal_nilai`
--

INSERT INTO `siswa_soal_nilai` (`kd`, `jadwal_kd`, `siswa_kd`, `siswa_noreg`, `siswa_nama`, `jml_benar`, `jml_salah`, `waktu_mulai`, `waktu_proses`, `waktu_akhir`, `skor`, `postdate`, `waktu_selesai`, `jml_soal_dikerjakan`, `revisi_postdate`, `revisi`) VALUES
('6f4f57e75431b3e5d10a87848cf3c3d9', '2c71ab8b8695560918004c724a6aa246', '8ef055edca2ad2f9e75917bd9ece25c5', NULL, NULL, '1', '4', '2022-01-06 11:33:43', '2022-01-06 11:47:44', '2022-01-06 11:38:43', '20', '2022-01-06 11:47:44', '2022-01-06 11:34:21', '5', NULL, 'false'),
('5d5b3856b55df92642f7c18cbdac3ebf', 'bfc87cf50cbb5a62d3e1ef961d854212', '8ef055edca2ad2f9e75917bd9ece25c5', NULL, NULL, '1', '4', '2022-01-06 11:35:25', '2022-01-06 11:35:42', '2022-01-06 11:40:25', NULL, '2022-01-06 11:35:42', '2022-01-06 11:35:38', '4', NULL, 'false'),
('ba8bba2c9e62922985eb2371016d6d5c', '6bc213d99845f7eaeba12abba80fa8e3', '8ef055edca2ad2f9e75917bd9ece25c5', NULL, NULL, '1', '4', '2022-01-06 11:50:49', '2022-01-06 11:51:42', '2022-01-06 11:55:49', NULL, '2022-01-06 11:51:42', '2022-01-06 11:51:08', '4', NULL, 'false'),
('b8acff45b0cbaa26cb84828cc648d51e', '375d075a0c9ba672746a725060d28809', '8ef055edca2ad2f9e75917bd9ece25c5', NULL, NULL, '0', '5', '2022-01-06 11:51:48', '2022-01-06 11:52:05', '2022-01-06 11:56:48', NULL, '2022-01-06 11:52:04', '2022-01-06 11:52:02', '4', NULL, 'false'),
('38f719cafd0f19bd9d9fd0bb8bbd55bb', '2c71ab8b8695560918004c724a6aa246', '0425202d47b6594106a0053b67cef0df', NULL, NULL, '1', '4', '2022-01-06 13:37:53', '2022-01-06 13:38:21', '2022-01-06 13:42:53', '20', '2022-01-06 13:38:20', '2022-01-06 13:38:17', '5', NULL, 'false'),
('cddde97a432f6b772b10f72cd8b5d09c', '2c71ab8b8695560918004c724a6aa246', 'e24d02a018a585b01ad39adfe2abd2b5', NULL, NULL, '2', '3', '2022-05-11 10:02:17', '2022-05-11 10:02:36', '2022-05-11 10:07:17', NULL, '2022-05-11 10:02:36', '2022-05-11 10:02:33', '4', NULL, 'false'),
('11c7114ee059dec45a7dc0ac9aa8bded', '041056c1a252372f14dfb60a50e4ec72', 'e24d02a018a585b01ad39adfe2abd2b5', NULL, NULL, '1', '4', '2022-05-11 10:02:58', '2022-05-11 10:03:18', '2022-05-11 10:07:58', NULL, '2022-05-11 10:03:17', '2022-05-11 10:03:14', '4', NULL, 'false'),
('ee485cc5ea3fdf57c9c6fb922851f778', '375d075a0c9ba672746a725060d28809', 'e24d02a018a585b01ad39adfe2abd2b5', NULL, NULL, '0', '5', '2022-05-11 15:41:27', '2022-05-11 15:41:34', '2022-05-11 15:46:27', NULL, '2022-05-11 15:41:34', '2022-05-11 15:41:34', '1', NULL, 'false');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_log_login`
--

CREATE TABLE `user_log_login` (
  `kd` varchar(50) NOT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_log_login`
--

INSERT INTO `user_log_login` (`kd`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `ipnya`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('fcb3022bca716abc37170d8cce2c31a7', '4c6fb7887334d77e428b7ae84f3ddbdf', '0032', '1', 'CALON', 'CALON', '127.0.0.1', 'true', '2022-05-11 16:10:11', '2022-01-11 11:24:15'),
('ebf3df2a4f6c3611bd38ffaea99a9b82', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '127.0.0.1', 'true', '2022-05-11 16:10:11', '2022-01-11 11:22:06'),
('0e698c083f4b6575171213380beacfa7', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '103.105.27.119', 'true', '2022-05-11 16:10:11', '2022-01-11 11:28:47'),
('8b5eadb2e34877a00e0dc098ae312eff', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '103.105.27.93', 'true', '2022-05-11 16:10:11', '2022-01-11 17:36:01'),
('d17a6c9d349152efd90e536710ad3106', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '118.99.112.2', 'true', '2022-05-11 16:10:11', '2022-01-27 12:02:01'),
('e2156e3c4b5dc43c07318c46a55d8b8a', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '118.99.112.2', 'true', '2022-05-11 16:10:11', '2022-01-27 12:02:51'),
('03bde8c79a92f33ade8666b922057f8a', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '118.99.112.2', 'true', '2022-05-11 16:10:11', '2022-01-27 13:04:35'),
('b53de7a14ef5ab9ecbea4ca93f03d5b7', 'e24d02a018a585b01ad39adfe2abd2b5', '0042', 'coba1', 'CALON', 'CALON', '127.0.0.1', 'true', '2022-05-11 16:10:11', '2022-05-11 10:02:06'),
('645773b06e4e213cb8ab75ac4bf0cdcc', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '127.0.0.1', 'true', '2022-05-11 16:10:11', '2022-05-11 10:03:35'),
('b368e08f3c55fe2bb8178e3a352f93e1', 'e24d02a018a585b01ad39adfe2abd2b5', '0042', 'coba1', 'CALON', 'CALON', '127.0.0.1', 'true', '2022-05-11 16:10:11', '2022-05-11 15:41:17'),
('abd5c50ffd02ecb85c57282365ec36cb', 'e807f1fcf82d132f9bb018ca6738a19f', 'ADMIN', 'ADMIN', 'ADMIN', 'Admin', '127.0.0.1', 'true', '2022-05-11 16:10:11', '2022-05-11 15:41:46');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `adminx`
--
ALTER TABLE `adminx`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`usernamex`,`passwordx`);

--
-- Indeks untuk tabel `cp_visitor`
--
ALTER TABLE `cp_visitor`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_jadwal`
--
ALTER TABLE `m_jadwal`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`durasi`),
  ADD KEY `akuindex2` (`no`,`soal_jml`),
  ADD KEY `akuindex3` (`postdate`);

--
-- Indeks untuk tabel `m_soal`
--
ALTER TABLE `m_soal`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`no`,`kunci`),
  ADD KEY `akuindex2` (`jadwal_kd`,`postdate`);

--
-- Indeks untuk tabel `m_soal_filebox`
--
ALTER TABLE `m_soal_filebox`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_tapel`
--
ALTER TABLE `m_tapel`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`tahun1`,`tahun2`);

--
-- Indeks untuk tabel `nomer2022`
--
ALTER TABLE `nomer2022`
  ADD PRIMARY KEY (`noid`),
  ADD UNIQUE KEY `noid` (`noid`),
  ADD KEY `noid_2` (`noid`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`usernamex`,`passwordx`),
  ADD KEY `akuindex2` (`noreg`,`nama`),
  ADD KEY `akuindex3` (`kelamin`,`lahir_tmp`),
  ADD KEY `akuindex4` (`tapel`),
  ADD KEY `akuindex5` (`postdate`);

--
-- Indeks untuk tabel `siswa_rekap`
--
ALTER TABLE `siswa_rekap`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`siswa_kd`),
  ADD KEY `akuindex2` (`siswa_noreg`,`siswa_nama`),
  ADD KEY `akuindex3` (`skor`),
  ADD KEY `mapel` (`mapel`);

--
-- Indeks untuk tabel `siswa_soal`
--
ALTER TABLE `siswa_soal`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`jadwal_kd`,`siswa_kd`,`soal_kd`),
  ADD KEY `akuindex2` (`jawab`,`kunci`,`benar`,`postdate`);

--
-- Indeks untuk tabel `siswa_soal_nilai`
--
ALTER TABLE `siswa_soal_nilai`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `akuindex` (`kd`,`jadwal_kd`,`siswa_kd`),
  ADD KEY `akuindex2` (`siswa_noreg`,`siswa_nama`,`jml_benar`,`jml_salah`),
  ADD KEY `akuindex3` (`waktu_mulai`,`waktu_proses`,`waktu_akhir`,`skor`),
  ADD KEY `akuindex4` (`postdate`,`waktu_selesai`,`jml_soal_dikerjakan`);

--
-- Indeks untuk tabel `user_log_login`
--
ALTER TABLE `user_log_login`
  ADD PRIMARY KEY (`kd`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `nomer2022`
--
ALTER TABLE `nomer2022`
  MODIFY `noid` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
